Please download manually the files from
http://supernovae.in2p3.fr/sdss_snls_jla/jla_likelihood_v6.tgz

Extract the arxiv, and copy all files from
`jla_likelihood_v6/data/`
to the current directory (`data/JLA/`, inside the montepython root
directory.
