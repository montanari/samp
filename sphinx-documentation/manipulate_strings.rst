Manipulate strings module
=========================

.. automodule:: manipulate_strings
    :members:
    :undoc-members:
    :show-inheritance:
