Cosmo Hammer module
===================

.. note::

   This is still an experimental feature in |MP|.

.. automodule:: cosmo_hammer
    :members:
    :undoc-members:
    :show-inheritance:

.. |MP| replace:: *SAMP*
