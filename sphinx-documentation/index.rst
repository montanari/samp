.. Monte Python documentation master file, created by
   sphinx-quickstart on Thu Mar  7 14:13:29 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation
=============

.. note::

   These documentation is based on the one originally provided by
   Benjamin Audren for the official *Monte Python* code. Some section
   has been adapted, others are *verbatim* re-distribution.

.. toctree::
   :maxdepth: 2

   getting_started
   example
   likelihoods
   external
   documentation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
