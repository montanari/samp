Add derived module
==================

.. automodule:: add_derived
    :members:
    :undoc-members:
    :show-inheritance:
