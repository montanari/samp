Nested Sampling module
======================

.. note::

   This feature is not implemented in |MP| yet, please refer to the
   upstream Monte Python code.

.. automodule:: nested_sampling
    :members:
    :undoc-members:
    :show-inheritance:

.. |MP| replace:: *SAMP*
