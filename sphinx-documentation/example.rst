Example of a complete work session
==================================

Let's first create a few folders within a main working directory:

:code:`$ mkdir chains`
  to store all the chains.

:code:`$ mkdir chains/hubble_spline`
  if the first run is based on the ``hubble_spline`` likelihood
  proposed in the :code:`example.param` file.

:code:`$ mkdir input`
  to store all my input files.

:code:`$ mkdir scripts`
  to store all the scripts to run the code, e.g., from a cluster.

Copy :code:`example.param` in the input folder and edit it if needed:

.. code::

  $ cp example.param input/

Launch a short chain with

.. code::

  $ montepython run -p input/example.param -o chains/example -N 5

The evolution of the initialization of the code is shown on screen. A
chain and a :code:`log.param` are written in the
:code:`chains/example/log.param` directory. A new chain can then be
launched with the same command. The second chain is automatically
created with number 2 instead of 1. The input file is no longer
required as the information is stored in :code:`log.param`:

.. code::

  $ montepython run -o chains/example -N 5

When the runs have stopped, they can be analysed with

.. code::

  $ montepython info chains/example --want-cov

Running without an initial covariance matrix, the results would
probably be bad, with a very low acceptance rate and few points. In
this case, the newly generated covariance matrix
:code:`chains/example/example.covmat` can be used as the initial one
determining the proposal distribution in a new, independent chain
folder. Let's copy all the files related to the preliminary chain into
a subfolder corresponding to the first, preliminary run:

.. code::

  mkdir chains/example/r1
  mv chains/example/* chains/example/r1

The script can be launched again as before adding the option

.. code::

  -c chains/example/r1/example.covmat

When this second run is finished, it can be analyses with

.. code::

  montepython info chains/example/example/2012-10-27_5001*

If all the Gelman-Rubin R-1 numbers are small (typically
:math:`<0.05`) and the plots look nice, the analysis is complete.

The acceptance rate is stored under :code:`chains/example/example.log`
file. In a case with nearly gaussian posterior (i.e. nearly
ellipsoidal contours), an acceptance rate :math:`<0.2` or :math:`>0.3`
can be considered as bad. In other cases, even 0.1 might be the best
achievable. If the acceptance rate is bad, a new run with an improved
covariance matrix is needed in order to converge quicker. The last
covariance matrix can be copied to a :code:`chains/example/r2` folder
and uses for the next run. If the acceptance rate is good but the
chains are not well converged because they are simply too short, then
a new run with the same covariance matrix :code:`lcdm_run1.covmat`
should be launched: in this way, since the proposal density is frozen
since the second run, the second and third runs can be safely analysed
altogether.

To spare more time and avoid a new burn-in phase, the following
solutions are also possible (however, it should always be checked that
the posterior does not depend on the initial guesses for best fits and
covariance matrices):

* Before launching a new run, the input mean value of each parameter
  can be set to the best-fit value found in the previous run.  The
  runs will then start from the best-fit value plus or minus the size
  of the first jump drown from the covariance matrix, and avoid
  burn-in. Since this requires changes to the input values, the run
  must take place under a new output directory, or the ``log.param``
  file must be moved into another (sub)folder.

* The new chains can be restarted from the previous chains using the
  :code:`-r` command line option. The name of previous chains can be
  written after :code:`-r` manually or through a script.

* The chains can be restarted from the best-fit found previously,
  using the :code:`-b` command line option, specifying the
  :code:`.bestfit` file to use.

Alternatively, preliminary chains can be obtained by reducing the
jumping factor with the option :code:`-f 1.5` (default 2.4). Note that
if the option is set to :code:`-f 0`, the likelihood is computed
exactly at the input values (useful to check, e.g., the likelihood at
a specific point in parameters space).

Finally, the plots can be customized through a file
:code:`plot_files/example.plot` passed through the :code:`-extra`
command line option. Summary latex tables are also automatically
produced.


Running on a cluster
^^^^^^^^^^^^^^^^^^^^

To launch longer runs on a cluster or on a powerful desktop, the
syntax of the script depends on the cluster. In the simplest case it
will only contain some general commands concerning the job name, time
limit etc., and the command line above, but with increased the number
of steps (usually roughly between 10000 or 100000 steps, depending on
the convergence). On some cluster, the chain file is created
immediately in the output directory at start up. In this case, the
automatic numbering of chains proposed by |MP| will be satisfactory.

.. warning::

  On some clusters, the automatic numbering will conflict when the chains
  are created too fast. Please look at the section on how to use
  :code:`mpi_run` for guidance

In other clusters, the chains are created on a temporary file, and
then copied at the end to the output file. In this case there is a
risk that chain names are identical and clash. The chain name should
be related to the job number, with an additional command line
:code:`--chain_number $JOBID`. On some cluster :code:`$JOBID` is a
string, but the job number can be extracted with a line like
:code:`export JOBNUM="$(echo $PBS_JOBID|cut -d'.' -f1)"`, and passed
to |MP| as :code:`--chain_number $JOBNUM`.

.. note::

   If using the Planck likelihood, the following line should be added to
   the script (before calling |MP|):

   .. code::

      source /path/to/my/plc/bin/clik_profile.sh


The jobs can then be submitted to the cluster. If the cluster creates
the chains too fast, there might be conflicts in the chain
names. Hence, it is good to add (manually or programmatically) some
latency before launching two subsequent chains.

Jobs can also be launched with :code:`mpi`. The syntax isunchanged,
except that the whole command starts with :code:`mpirun` or
:code:`mpiexec`, depending on the installation. For instance:

.. code::

  mpirun -np 4 montepython run -o chains/...

will simply launch 4 chains, each using the environment variable
:code:`$OMP_NUM_THREADS` for the number of cores to compute *Class*.


.. |CLASS| replace:: *CLASS*
.. |MP| replace:: *SAMP*

.. rubric:: Footnotes
