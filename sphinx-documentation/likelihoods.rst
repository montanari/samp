Existing likelihoods, and how to create new ones
================================================


This page is intended to explain in more concrete terms the
information contained in the :doc:`likelihood_class` documentation.
More specifically, you should be able to write new likelihood files
and understand the structure of existing ones.

One likelihood is one directory, one :code:`.py` and one :code:`.data` file
----------------------------------------------------------------------------

|MP| calls likelihood codes implemented in the
:code:`montepython/likelihoods/` sub-package. Each likelihood
implemented properly in that directory is ready to be used if
mentioned in the input parameters file.

A precise syntax must be respected. Each likelihood is associated to a
name, e.g. :code:`hubble_spline` or :code:`JLA_spline` (the name is
case-sensitive). This name is used:

* To call the likelihood in the input file, e.g.,
  :code:`data.experiments = ['hubble_spline', ...]`.

* To name the directory of the likelihood, e.g.,
  :code:`montepython/likelihoods/hubble_spline/`.

* To name the input data file describing the characteristics of the
  experiment,
  :code:`montepython/likelihoods/hubble_spline/hubble_spline.data`
  (this file can point to raw data files located in the :code:`data`
  directory located in the main |MP| folder).

* To name the class declared in
  :code:`montepython/likelihoods/hubble_spline/__init__.py` and used
  also in
  :code:`montepython/likelihoods/hubble_spline/hubble_spline.data`.

When implementing new likelihoods, the previous rules must be
followed.


Creating new likelihoods from scratch
-------------------------------------

To create your own likelihood files, the best is to look at existing
examples and follow them. Here are some general indications.

Your customised likelihood should inherit from generic likelihood
properties through:

.. code::

  from montepython.likelihood_class import Likelihood
  class my-likelihood(Likelihood):


Implementing the likelihood amounts in developing in the python file
:code:`my-likelihood.py` the properties of two essential functions,
:code:`__init__` and :code:`loglkl`. But you don't need to code
everything from scratch, because the generic :class:`likelihood
<likelihood_class.Likelihood>` already knows the most generic steps.
The previous link will give you all the functions defined from this
base class, that your daughter class will inherit from. Here follows a
detailled explanation about how to use these.

One thing is that you don't need to write from scratch the parser
reading the :code:`.data` file: this will be done automatically at the
beginning of the initialization of your likelihood. Consider that any
field defined with a line in the :code:`.data` file, e.g.
:code:`my-likelihood.variance = 5`, are known in the likelihood code:
in this example you could write in the python code something like
:code:`chi2+=result**2/self.variance`.

If *Class* is not installed, Python packages such as Astropy can be
imported in the likelihood code to compute cosmological quantities
(see :ref:`ext-python`). Furthermore, you don't need either to write
from scratch an interface with *Class*, if you wish to use it as
cosmological module.  You just need to write somewhere in the
initialization function some specific parameters that should be passed
to *Class*. For instance, if you need the matter power spectrum, write

.. code::

  self.need_cosmo_arguments(data, {'output':'mPk'})

that uses the method :func:`need_cosmo_arguments
<likelihood_class.Likelihood.need_cosmo_arguments>`. If this
likelihood is used, the field :code:`mPk` will be appended to the list
of output fields (e.g. :code:`output=tCl,pCl,mPk`), unless it was
already there. If you write

.. code::

  self.need_cosmo_arguments(data, {'l_max_scalars':3300})

the code will check if :code:`l_max_scalars` was already set at least
to 3300, and if not, it will increase it to 3300. But if another
likelihood needs more it will be more.

You don't need to redefine functions like for instance those defining
the role of nuisance parameters (especially for CMB experiments).  If
you write in the :code:`.data` file

.. code::

  my-likelihood.use_nuisance           = ['N1','N2']

the code will know that this likelihood cannot work if these two
nuisance parameters are not specified  in the parameter input file
(they can be varying or fixed; fix them by writing a 0 in the sigma
entry). If you try to run without them, the code will stop with an
explicit error message.  If the parameter :code:`N1` has a top-hat
prior, no need to write it: just specify prior edges in the input
parameter file. If :code:`N2` has a gaussian prior, specify it in the
:code:`.data` file, e.g.:

.. code::

  my-likelihood.N2_prior_center  = 1
  my-likelihood.N2_prior_variance = 2

Since these fields refer to pre-defined properties of the likelihood,
you don't need to write explicitly in the code something like
:code:`chi2 += (N2-center)**2/variance`, adding the prior is done
automatically. Finally, if these nuisance parameters are associated to
a CMB dataset, they may stand for a multiplicative factor in front of
a contamination spectrum to be added to the theoretical
:math:`C_{\ell}`'s. This is the case for the nuisance parameters of
the :code:`acbar`, :code:`spt` and :code:`wmap` likelihoods delivered
with the code, so you can look there for concrete examples. To assign
this role to these nuisance parameters, you just need to write

.. code::

  my-likelihood.N1_file = 'contamination_corresponding_to_N1.data'

and the code will understand what it should do with the parameter
:code:`N1` and the file
:code:`data/contamination_corresponding_to_N1.data`. Optionally, the
factor in front of the contamination spectrum can be rescaled by a
constant number using the syntax:

.. code::

  my-likelihood.N1_scale = 0.5


.. |MP| replace:: *SAMP*
