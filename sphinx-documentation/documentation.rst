Source code documentation
=========================

This documentation is automatically extracted from the code.

.. toctree::
   :maxdepth: 2

   add_derived
   analyze
   cosmo_hammer
   data
   importance_sampling
   initialise
   io_mp
   likelihood_class
   manipulate_matrices
   manipulate_strings
   mcmc
   MontePython
   nested_sampling
   parser_mp
   polytools
   prior
   run
   sampler
   splinetools
