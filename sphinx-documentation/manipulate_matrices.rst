Manipulate matrices module
==========================

.. automodule:: manipulate_matrices
    :members:
    :undoc-members:
    :show-inheritance:
