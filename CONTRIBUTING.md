Commit
------
When sending contributions formatted with `git` (both in the form of
small patches or push requests, see below), please provide useful
commit messages, stating a reason for the modification.

A commit message should be formatted in the following way:

- Line 1: Short description of the overall change (less than 50
  characters). This is the title of the commit, so there should be no
  starting star or final dot.

- Line 2: Empty line.

- (*optional*) Line 3: Paragraph or list with a detailed description
  of what changed. It ends with an empty line.

Here is an example:
```text
hubble_spline/: Add knots in log.param

* hubble_spline/: Knots were correctly set, but they were no printed
  in the log.param.

* hubble_spline/: Add an option to either set manually the spline
  knots, or to set them automatically in the likelihood.

The likelihood has now all the basic parameters.

```

Submit patches
--------------

Please create patches using `git format-patch`.

Here is the
[suggested workflow](http://orgmode.org/worg/org-contribute.html#patches):

```shell
~$ git pull                 # make sure your repo is up to date
~$ git branch my-changes    # create a new branch from master
~$ git checkout my-changes  # switch to this new branch
```

make some changes (1)

```shell
~$ git commit -a -m "This is change (1)"  # Commit your change
```

make another change (2)

```shell
~$ git commit -a -m "This is change (2)"  # Commit your change
~$ git format-patch master                # Creates two patches
```

Then two patches for your two commits are ready to be sent to the
issue tracker.


Develop
-------

For substantial contributions, we follow the Integration-Manager
Workflow described in Section 5 of the [Pro
Git](https://git-scm.com/book/en/v2) book:

1. The project maintainer pushes to their public repository.
2. A contributor clones that repository and makes changes.
3. The contributor pushes to their own public copy.
4. The contributor sends the maintainer an email asking them to pull
   changes.
5. The maintainer adds the contributor’s repo as a remote and merges
   locally.
6. The maintainer pushes merged changes to the main repository.

Section 5.2 of [Pro Git](https://git-scm.com/book/en/v2) explains in
details how to contribute to a project.

Let's assume that a new project `samp-janedoe` is created for the
`janedoe` user and hosted by some git repository (for example,
GitLab). Clone the original repository and add a remote to the new
personal repository:

```shell
$ git clone git@gitlab.com:montanari/samp.git
$ cd samp
$ git remote add janedoe git@gitlab.com:janedoe/samp-janedoe.git
$ git push janedoe master
```

Before starting a new contribution, checkout updates from the master
branch. Then, create a new branch with the new contribution:

```shell
$ git checkout master
$ git pull
$ git checkout -b bug-gausian-prior    # Descriptive name for the modification
```

Finally, modify the code and commit the changes:

```shell
$ git add montepython/file.py
$ git commit
$ git push janedoe bug-gausian-prior
```

Once the commit is merged into the master branch, the branch created
for the modification can be removed.
