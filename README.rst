****
SAMP
****

Monte Carlo sampler for cosmology
#################################

*SAMP* (SAMP's Adapted Monte Python) is a Markov chain Monte
Carlo (MCMC) sampler for cosmology.

For a generic cosmological MCMC sampler interfaced with the `CLASS
<http://class-code.net/>`_ Boltzmann solver, please refer to the
original `Monte Python
<https://github.com/baudren/montepython_public>`_ code. The aim of
*SAMP* is to provide a ready-to-run software to reproduce the results
obtained in the respective paper listed at the end of this README.


Prerequisites
=============

We suggest to install the `Anaconda
<https://www.continuum.io/downloads>`_ distribution based on **Python
version 2.7**. Alternatively, here is a detailed list of Python
software requirements:

* Python version 2.7.

* Numpy.

* Scipy.

* *[optional]* Matplotlib, if you wish to generate plots when
  analyzing the chains.

* *[optional]* the `CLASS <https://github.com/lesgourg/class_public>`_
  Boltzmann solver is required by some likelihood. Please follow its
  instructions to install the *Classy* Python wrapper, which allows
  interfacing it with *SAMP*. Note that *SAMP* allows to import
  generic Python modules other than *Classy* to compute simple
  cosmological quantities. For instance, spline and polynomial fits to
  JLA and Hubble parameter data do not require a CLASS installation.


Installation
============

Download and extract SAMP, or clone its git repository.

.. code-block:: shell

   git clone git@gitlab.com:montanari/samp.git

A ``setup.py`` is provided for use with standard Python software
installation utilities. If in doubt, a simple installation script is
also provided. Enter the SAMP main directory from command line and
type:

.. code-block:: shell

   cd samp/
   sh install.sh

Similarly, the code can be uninstalled as

.. code-block:: shell

   sh uninstall.sh

After installation, an executable ``montepython`` is provided to be
called from command line, and the ``montepython`` package can be
imported from other Python code. To verify the proper installation,
display the SAMP version:

.. code-block:: shell

   montepython --version

The version (e.g., 2.2.1.samp0.1) shows first the Monte Python
version upon which the code is based (e.g., 2.2.1), and then the SAMP
version relative to that branch (e.g., samp0.1).


Usage
=====

Create or adapt a configuration file (such as ``default.conf``, see
the file ``templates/default.conf.template`` for more information)
within some project directory. The file should specify, e.g., the data
folder path (if it differs from the standard directory stored in the
main SAMP folder) or the path to an external cosmological code such as
CLASS (if needed). If CLASS (or other cosmological code needing a
particular interface) is not required, do not specify a
``path['cosmo']`` field in the configuration file. In the simplest
case, ``default.conf`` is just an empty file.

The main options for the command line interface are ``run`` to launch
MCMC chains, and ``info`` to analyze existing chains. Help messages
are displayed as follows.

.. code::

    montepython --help
    montepython run --help
    montepython info --help

For the ``run`` option, at minimum you should provide an output folder
(through the ``-o`` flag) and a parameter file (``-p``). An example of
parameter file is provided under ``templates/example.param``.

A typical call would then be:

.. code::

    montepython run -o chains/example -p example.param

If non existent, the ``chains/example/`` folder will be created, and a
run with the number of steps described in ``example.param`` will be
started. To run a chain with more steps, say a hundred:

.. code::

    montepython run -o chains/example -p example.param -N 1000

If you want to analyse the run, then just type

.. code::

    montepython info chains/example/

Some module of the code is also well-adapted to be imported from
Python programs. For instance

.. code-block:: python

   from montepython import splinetools as st    # Cubic spline interpolation
   from montepython import polytools as pt      # Polynomial interpolation


Development
===========

A development workflow guide is provided by the ``CONTRIBUTING.md``
file.

As in the case of Monte Python, SAMP can easily call the Classy
interface to the CLASS Boltzmann solver. Classy provides easy access
to computational expensive observables such as angular power
spectra. However, this requires the installation of the CLASS code,
its wrapper, and a proper ``.conf`` SAMP configuration
file. Differently from Monte Python, SAMP can run without a Classy
installation. Hence, if a likelihood relies only on simple
cosmological quantities (e.g., distances or Hubble parameter), a
module such as `Astropy <http://www.astropy.org/>`_ can be imported
instead of Classy. (The package ``montepython/cosmolib/`` is also
distributed together with SAMP, but we suggest to import Astropy
instead.)

After modifying the source files, the code must be installed again for
the changes to take effect through the ``montepython`` command. When
developing SAMP it is the more convenient to call directly the script
``montepython/MontePython.py`` (that does not require installation)
instead of the command ``montepython``. For instance, from the main
SAMP folder:

.. code-block:: shell

   montepython/MontePython.py --version

Each change to the main source files should be accompanied by a
respective extension to the unit test under the folder ``tests/``
(direct changes the the likelihoods may be excluded if it is too
difficult to implement automatized tests in this framework). Note that
``test_montepython.py`` should be run only through the Makefile and
requires CLASS to be installed and its path to be defined under
``default.conf``, as well as MultiNest (if not, the respective tests
will fail). The other test files can be run directly after that the
changes to the respective modules are installed.


Support
=======

Please refer to the ``Samp.pdf`` documentation distributed with the
code for more details about SAMP usage and structure.

You may also want to check the `wiki
<https://github.com/baudren/montepython_public/wiki>`_ and the `forum
<https://github.com/baudren/montepython_public/issues>`_ of the
original Monte Python code (please do not report bugs specific to SAMP
there).

To report bugs related to modifications introduced in SAMP, please add
an item to the issue tracker on the git repository page and provide an
`effective description
<http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>`_. To know if
a given modification has been introduced in SAMP, please consult the
``git`` commits relative to that file.


Copying
=======

This program is free software: you can redistribute it and/or modify
it under the terms of the MIT (Expat) License. A copy of the license
is provided in the ``LICENSE.txt`` file.

A list of contributors is available in the ``AUTHORS.txt`` file.

Data sets distributed together with the code are subject to their
respective licenses.

Cite
====

When using *SAMP* in a publication, please acknowledge the code by
citing the following paper:

- F. Montanari and S. Räsänen, *Backreaction and FRW consistency
  conditions*, `arXiv:1709.06022
  <https://arxiv.org/abs/1709.06022>`_ [astro-ph.CO]. [`Bibtex
  <https://inspirehep.net/record/1624149/export/hx>`_]

SAMP relies on *Monte Python*, please cite also the relative paper:

- B. Audren, J. Lesgourgues, K. Benabed and S. Prunet, *Conservative
  Constraints on Early Cosmology: an illustration of the Monte Python
  cosmological parameter inference code*, `arXiv:1210.7183
  <https://arxiv.org/abs/1210.7183>`_ [astro-ph.CO]. [`Bibtex
  <https://inspirehep.net/record/1193750/export/hx>`_]
