import unittest

import numpy as np
from scipy import interpolate
from scipy.optimize import curve_fit

import montepython.splinetools as st
import test_polytools as tpt


class TestFLRW(unittest.TestCase):
    """Test cosmological functions given a spline fit to LCDM. For a given
    function, the same relative tolerance is tested over a broad range
    of redshifts. Hence, only significant changes are identified.

    """

    def setUp(self):
        zmin = 1.e-3
        zmax = 1.5
        self.redshifts = np.linspace(zmin, zmax, 10)

        self.Omega_m0 = 0.3
        self.cosmo = tpt.FlatFLRW(self.Omega_m0, self.redshifts)

        # Find 3rd order spline fit coefficients
        dL_k, __ = curve_fit(self._get_d_k4, self.redshifts,
                                  self.cosmo.dL)
        dL_k1, dL_k2, dL_k3, dL_k4 = dL_k
        dist_pack = self._get_distance_all(self.redshifts, dL_k1,
                                           dL_k2, dL_k3, dL_k4)
        self.dL, self.d1L, self.d2L = dist_pack

    def tearDown(self):
        pass

    def _get_knots(self, N):
        start = np.log10(self.redshifts[0])
        stop = np.log10(self.redshifts[-1])
        knots = np.logspace(start, stop, N, base = 10.0)
        return knots

    # def _get_d_k3(self, redshifts, d_k1, d_k2, d_k3):
    #     """Third order spline for d=D/D0, with d(z=0)=0 and d'(z=0)=1."""
    #     y = np.array([d_k1, d_k2, d_k3])
    #     x = self._get_knots(len(y))
    #     d_0 = 0.
    #     d1_0 = 1.
    #     d, __ = st.spline_p0(redshifts, x, y, d_0, deriv_0=(d1_0, 1),
    #                          deriv_f=(self.cosmo.d1L[-1], 1),
    #                          want_y1=False)
    #     return d

    def _get_d_k4(self, redshifts, d_k1, d_k2, d_k3, d_k4):
        """Third order spline for d=D/D0, with d(z=0)=0 and d'(z=0)=1."""
        y = np.array([d_k1, d_k2, d_k3, d_k4])
        x = self._get_knots(len(y))
        d_0 = 0.
        d1_0 = 1.
        d, __ = st.spline_p0(redshifts, x, y, d_0, deriv_0=(d1_0, 1),
                             deriv_f=(self.cosmo.d1L[-1], 1),
                             want_y1=False)
        return d

    def _get_distance_all(self, redshifts, *args):
        """Distance and its first, second derivatives at given
        redshifts. Spline parameters must be passed as *args.

        """
        y = np.array(args)
        x = self._get_knots(len(args))
        d_0 = 0.
        d1_0 = 1.

        d, d1_k, d2_k = st.spline_p0(redshifts, x, y, d_0,
                                     deriv_0=(d1_0, 1),
                                     deriv_f=(self.cosmo.d1L[-1], 1),
                                     want_y1=True)

        # Interpolate the derivatives.
        d1_interp = interpolate.interp1d(x, d1_k, kind='linear')
        d1 = np.array([d1_interp(z) for z in redshifts])

        d2_interp = interpolate.interp1d(x, d2_k, kind='linear')
        d2 = np.array([d2_interp(z) for z in redshifts])

        return d, d1, d2

    def test_dL(self):
        """Spline distance."""
        rtol = 1.e-2
        isgood = np.all(abs(1.-self.dL/self.cosmo.dL) < rtol)
        self.assertTrue(isgood)

    def test_d1L(self):
        """Spline distance, first derivative."""
        rtol = 1.e-1
        isgood = np.all(abs(1.-self.d1L/self.cosmo.d1L) < rtol)
        self.assertTrue(isgood)

    # def test_d2L(self):
    #     """Spline distance, second derivative. It does not make much
    #     sense to compare it.
    #     """
    #     rtol = 1.e-1
    #     print abs(1.-self.d2L/self.cosmo.d2L)
    #     self.assertTrue(True)


class TestExp(unittest.TestCase):
    """Check if the spline procedure spline_p0() recovers an exponential
    function.

    """

    def setUp(self):
        nknots = 10
        self.x_1 = 0.1
        self.x_f = 10.
        self.x = np.linspace(self.x_1, self.x_f, nknots)
        self.y = self._func(self.x)

        # Define the functions at the point x=0 automatically added by
        # spline_p0().
        self.x_0 = 0.
        self.y_0 = self._func(0.0)
        self.y1_0 = self._func(0.0, deriv=1)
        self.y2_0 = self._func(0.0, deriv=2)

        # Final point.
        self.y_f = self._func(self.x_f)
        self.y1_f = self._func(self.x_f, deriv=1)
        self.y2_f = self._func(self.x_f, deriv=2)

        nspl = 2 * nknots
        # Compute the spline at the following values.
        self.z = np.linspace(self.x_0, self.x_f, nspl)
        # Exact answer.
        self.y_at_z = self._func(self.z)

    def tearDown(self):
        pass

    def _func(self, x, deriv=None):
        """Return the testing function or, if requested, its first or second
        derivative.

        """
        if not deriv:
            return np.exp(-x/3.0)
        elif deriv==1:
            return -np.exp(-x/3.0) / 3.0
        elif deriv==2:
            return np.exp(-x/3.0) / 9.0

    def testScipy(self):
        """Compare to scipy result."""
        rtol = 3e-16

        # Scipy. Since spline_p0() adds automatically a point at x=0,
        # we need to add it manually to the arrays passed to Scipy.
        xall = np.append([self.x_0], self.x)
        yall = np.append([self.y_0], self.y)
        f_spy = interpolate.CubicSpline(xall, yall,
                                        bc_type=((1, self.y1_0),
                                                 (1, self.y1_f)))
        y_spy = f_spy(self.z)

        # Splinetools.
        y_spl, __ = st.spline_p0(self.z, self.x, self.y, self.y_0,
                                 deriv_0=(self.y1_0, 1),
                                 deriv_f=(self.y1_f, 1),
                                 want_y1=False)

        isgood = np.all(abs(1.-y_spl/y_spy) < rtol)
        self.assertTrue(isgood)

    def testFirst(self):
        """Set first derivatives as boundary conditions."""
        rtol = 1e-4

        y_spl, __ = st.spline_p0(self.z, self.x, self.y, self.y_0,
                                 deriv_0=(self.y1_0, 1),
                                 deriv_f=(self.y1_f, 1),
                                 want_y1=False)

        isgood = np.all(abs(1.-y_spl/self.y_at_z) < rtol)
        self.assertTrue(isgood)

    def testSecond(self):
        """Set second derivatives as boundary conditions."""
        rtol = 1.1e-4

        y_spl, __ = st.spline_p0(self.z, self.x, self.y, self.y_0,
                                 deriv_0=(self.y2_0, 2),
                                 deriv_f=(self.y2_f, 2),
                                 want_y1=False)

        isgood = np.all(abs(1.-y_spl/self.y_at_z) < rtol)
        self.assertTrue(isgood)


if __name__ == '__main__':
    unittest.main()
