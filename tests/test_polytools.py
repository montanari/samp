import unittest
import montepython.polytools as pt
import numpy as np
import scipy.integrate as integrate
from scipy.optimize import curve_fit

class FlatFLRW():
    def __init__(self, Omega_m0, redshifts):
        """Set dust plus vacuum energy cosmology.

        Parameters
        ----------
        Omega_m0 : float
            Matter density parameter today.
        redshifts : list
            List of redshifts at which to evaluate cosmological
            functions.
        """
        self.Omega_m0 = Omega_m0
        # Retrieve cosmological functions for the following redshifts.
        self.redshifts = redshifts
        self.set_cosmofunc_flatFLRW()

    def _efunc_flatFLRW(self,z):
        """Integrand of the comoving distance."""
        Omm = self.Omega_m0
        Ez = np.sqrt(Omm*(1.+z)**3. + 1-Omm)
        return Ez

    def _inv_efunc_flatFLRW(self,z):
        """Integrand of the comoving distance."""
        Ez = self._efunc_flatFLRW(z)
        return 1/Ez

    def _get_d_flatFLRW(self):
        """Comoving distance."""
        d = np.zeros(len(self.redshifts))
        for i,z in enumerate(self.redshifts):
            d[i] = integrate.quad(self._inv_efunc_flatFLRW, 0, z)[0]
        return d

    def _integrand_dP(self, z):
        d = integrate.quad(self._inv_efunc_flatFLRW, 0, z)[0]
        return 1. / self._efunc_flatFLRW(z) / d**2.

    def set_cosmofunc_flatFLRW(self):
        """Set cosmological functions (without dimensions):
        h, h_prime, dL, d1L, d2L, wtot.
        """
        zp1 = (1+self.redshifts)
        Omm = self.Omega_m0
        d = self._get_d_flatFLRW()

        self.h = self._efunc_flatFLRW(self.redshifts)
        self.h_prime = 3./2. * Omm * zp1**2. / self.h
        self.dL = zp1*d
        self.d1L = zp1/self.h + d
        self.d2L = 2./self.h - 3./2.*Omm*(zp1/self.h)**3
        self.wtot = 2./3.*zp1*self.h_prime/self.h-1.


class TestExactLCDM(unittest.TestCase):
    """Test cosmological functions given LCDM distance."""

    def setUp(self):
        self.Omega_m0 = 0.3
        #self.redshifts = np.array([0.,0.1,1.0,10.0,100.0])
        self.redshifts = np.linspace(0.,10.)
        self.cosmo = FlatFLRW(self.Omega_m0, self.redshifts)

    def tearDown(self):
        pass

    def test_wDtot(self):
        wDtot = pt.get_wDtot(self.redshifts, self.cosmo.dL,
                             self.cosmo.d1L, self.cosmo.d2L)
        for i in range(len(self.redshifts)):
            self.assertAlmostEqual(wDtot[i],self.cosmo.wtot[i])

    def test_wHtot(self):
        wHtot = pt.get_wHtot(self.redshifts, self.Omega_m0,
                             self.cosmo.dL, self.cosmo.d1L,
                             self.cosmo.d2L, self.cosmo.h)
        for i in range(len(self.redshifts)):
            self.assertAlmostEqual(wHtot[i],self.cosmo.wtot[i])

    def test_wD(self):
        wD = pt.get_wD(self.redshifts, self.Omega_m0, self.cosmo.dL,
                       self.cosmo.d1L, self.cosmo.d2L)
        for i in range(len(self.redshifts)):
            self.assertAlmostEqual(wD[i],-1.)

    def test_wH(self):
        wH = pt.get_wH(self.redshifts, self.Omega_m0,
                       self.cosmo.dL, self.cosmo.d1L,
                       self.cosmo.d2L, self.cosmo.h)
        for i in range(len(self.redshifts)):
            self.assertAlmostEqual(wH[i],-1.)


class TestPolySachs(unittest.TestCase):
    """Test cosmological functions given a polynomial fit to LCDM
    distance. For a given function, the same relative tolerance is
    tested over a broad range of redshifts. Hence, only significant
    changes are identified.

    """

    def setUp(self):
        zmin = 1.e-3
        zmax = 1.5
        self.redshifts = np.linspace(zmin,zmax,10)

        self.Omega_m0 = 0.3
        self.cosmo = FlatFLRW(self.Omega_m0, self.redshifts)

        # # # Find 4th order fit self.coefficients
        # popt, pcov = curve_fit(self._get_fourth_poly, self.redshifts,
        #                        self.cosmo.dL)
        # self.coeff = np.append(popt,[0])

        # Find 5th order fit self.coefficients.
        popt, pcov = curve_fit(self._get_fifth_poly, self.redshifts,
                               self.cosmo.dL)
        self.coeff = popt

        # Distances.
        self.fitdist = 'luminosity'
        dL_packed = pt.dL_poly(self.redshifts, self.coeff, 'luminosity',
                               deriv=True)
        self.dL_poly, self.d1L_poly, self.d2L_poly = dL_packed

        # Omega_m0.
        limits = (zmin, 2.5)
        z0 = pt.sachs_z0(self.coeff, limits, self.fitdist)
        self.Omega_m0_poly = pt.sachs_Omega_m(z0, self.coeff, self.fitdist)

        # Hubble.
        self.hubble_poly = pt.sachs_hubble(self.redshifts, self.coeff,
                                           self.Omega_m0_poly, self.fitdist)

        # Equations of state.
        self.wDtot_poly = pt.get_wDtot(self.redshifts, self.dL_poly,
                                       self.d1L_poly, self.d2L_poly)
        self.wHtot_poly = pt.get_wHtot(self.redshifts,
                                       self.Omega_m0_poly,
                                       self.dL_poly, self.d1L_poly,
                                       self.d2L_poly,
                                       self.hubble_poly)
        self.wD_poly = pt.get_wD(self.redshifts, self.Omega_m0_poly,
                                       self.dL_poly, self.d1L_poly,
                                       self.d2L_poly)
        self.wH_poly = pt.get_wH(self.redshifts, self.Omega_m0_poly,
                                       self.dL_poly, self.d1L_poly,
                                       self.d2L_poly,
                                       self.hubble_poly)

    def tearDown(self):
        pass

    def _get_fourth_poly(self, z, c2, c3, c4):
        """d=D/D0, with d(z=0)=0 and d'(z=0)=1."""
        return z + c2*z**2. + c3*z**3. + c4*z**4.

    def _get_fifth_poly(self,z, c2, c3, c4, c5):
        """d=D/D0, with d(z=0)=0 and d'(z=0)=1."""
        return z + c2*z**2. + c3*z**3. + c4*z**4. + c5*z**5.

    def test_dL(self):
        """Polynomial distance."""
        rtol = 3e-4
        isgood = np.all(abs(1.-self.dL_poly/self.cosmo.dL) < rtol)
        self.assertTrue(isgood)

    def test_d1L(self):
        """First derivative of polynomial distance."""
        rtol = 5e-4
        isgood = np.all(abs(1.-self.d1L_poly/self.cosmo.d1L) < rtol)
        self.assertTrue(isgood)

    def test_d2L(self):
        """Second derivative of polynomial distance."""
        rtol = 5e-2
        isgood = np.all(abs(1.-self.d2L_poly/self.cosmo.d2L) < rtol)
        self.assertTrue(isgood)

    def test_Omega_m(self):
        """Omega_m0 from Sachs equation, given the polynomial
        distance.
        """
        rtol = 1e-3
        isgood = np.all(abs(1.-self.Omega_m0_poly/self.Omega_m0) < rtol)
        self.assertTrue(isgood)

    def test_hubble(self):
        """Hubble parameter from Sachs equation, given the polynomial
        distance.
        """
        rtol = 1e-2
        isgood = np.all(abs(1.-self.hubble_poly/self.cosmo.h) < rtol)
        self.assertTrue(isgood)

    def test_hubble_const_kH(self):
        """Hubble parameter given the polynomial distance fit and a constant
        curvature parameter.
        """
        rtol = 1e-15
        kH = 0.
        hubble = pt.hubble_const_kH(self.redshifts, self.cosmo.dL,
                                    self.cosmo.d1L, kH)
        isgood = np.all(abs(1.-hubble/self.cosmo.h) < rtol)
        self.assertTrue(isgood)


    def test_wDtot_poly(self):
        """Equation of state wDtot."""
        rtol = 2.e-1
        isgood = np.all(abs(1.-self.wDtot_poly/self.cosmo.wtot) < rtol)
        self.assertTrue(isgood)

    def test_wHtot_poly(self):
        """Equation of state wHtot."""
        rtol = 4.e-1
        isgood = np.all(abs(1.-self.wHtot_poly/self.cosmo.wtot) < rtol)
        self.assertTrue(isgood)

    def test_wD_poly(self):
        """Equation of state wD."""
        rtol = 2.e-1
        isgood = np.all(abs(1.-self.wD_poly/(-1.)) < rtol)
        self.assertTrue(isgood)

    def test_wH_poly(self):
        """Equation of state wH."""
        rtol = 4.e-1
        isgood = np.all(abs(1.-self.wH_poly/(-1.)) < rtol)
        self.assertTrue(isgood)

    def test_kP(self):
        """Parallax distance sum rule."""
        rtol = 3e-2

        kP_poly = pt.kP_sachs(self.redshifts, self.coeff, self.Omega_m0,
                              self.fitdist)

        isgood = np.all(abs(kP_poly) < rtol)
        self.assertTrue(isgood)

    def test_sumrule_d(self):
        """Distance sum rule."""
        rtol = 3.e-4

        # Compute reference cosmology at different redshifts
        zl = np.array([self.redshifts[0]])
        cosmol = FlatFLRW(self.Omega_m0, zl)

        zs_list = self.redshifts[1:]
        cosmos = FlatFLRW(self.Omega_m0, zs_list)

        dl_lcdm = cosmol.dL / (1.+zl)
        ds_lcdm = cosmos.dL / (1.+zs_list)
        dls_lcdm = ds_lcdm - dl_lcdm

        # Compute distances according to polynomial fit and Sachs equation
        dl, ds_list, dls_list = pt.sachs_sumrule_d(zl, zs_list, self.coeff,
                                                   self.Omega_m0_poly,
                                                   self.fitdist)

        # Sum rule
        kS = pt.kS_sachs(dl, ds_list, dls_list)
        kS_lcdm = pt.kS_sachs(dl_lcdm, ds_lcdm, dls_lcdm)

        # Test against reference model
        isgood = np.all(abs(1.-dl/dl_lcdm) < rtol)
        self.assertTrue(isgood)

        isgood = np.all(abs(1.-ds_list/ds_lcdm) < rtol)
        self.assertTrue(isgood)

        isgood = np.all(abs(1.-dls_list/dls_lcdm) < rtol)
        self.assertTrue(isgood)

        # Test self-consistency of kS for flat FLRW
        for k in kS_lcdm:
            self.assertAlmostEqual(k,0.)

        rtol = 3.e-3
        isgood = np.all(kS < rtol)
        self.assertTrue(isgood)


class TestPolyHubble(unittest.TestCase):
    """Test cosmological functions given a 4th order polynomial fit to
    LCDM Hubble parameter. For a given function, the same relative tolerance
    is tested over a broad range of redshifts. Hence, only significant
    changes are identified.
    """

    def setUp(self):
        zmin = 0.
        zmax = 2.3
        self.redshifts = np.linspace(zmin,zmax,10)

        self.Omega_m0 = 0.3
        self.cosmo = FlatFLRW(self.Omega_m0,self.redshifts)

        # Find polynomial fit coefficients
        popt, pcov = curve_fit(self._get_poly, self.redshifts,
                               self.cosmo.h)
        self.coeff = popt

        # Hubble
        self.hubble_poly = pt.hubble_poly(self.redshifts, self.coeff)

    def tearDown(self):
        pass

    def _get_poly(self, z, ch1, ch2, ch3, ch4):
        """h=H/H0, with h(z=0)=1."""
        return 1. + ch1*z + ch2*z**2. + ch3*z**3. + ch4*z**4.

    def test_hubble(self):
        """Hubble parameter polynomial fit."""
        rtol = 1.e-4
        isgood = np.all(abs(1.-self.hubble_poly/self.cosmo.h) < rtol)
        self.assertTrue(isgood)


if __name__ == '__main__':
    unittest.main()
