"""
.. module:: polytools
   :synopsis: Common tools for polynomial likelihood.
.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

This module defines functions used to compute the SN polynomial likelihoods.

"""

import numpy as np
from scipy import integrate
from scipy import optimize
from scipy import signal

def get_coeff(coeff):
    """Read polynomial coefficients."""

    try:
        c2, c3, c4, c5 = coeff
    except:
        msg = "The polynomial requires four coefficients."
        raise TypeError(msg)

    return (c2,c3,c4,c5)


def hprime_zero(c2, fitdist):
    """Compute redshift derivative h'(0) from Sachs' equation given d_L(z)
    = (1+z)^conv * (z + c_2*z**2 + c_3*z**3 + ...).  The expression
    depends only on c2 and conv.

    Parameters
    ----------
    coeff: list
        Coefficients of the polynomial.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Return
    ------
    hprime_zero : float
        Value of d[h(z)]/dz in z=0.

    """
    dfactor = {'luminosity':0, 'angular':2, 'comoving':1}
    conv = dfactor[fitdist]

    return 2 - 2*c2 - 2*conv


def dL_poly(z, coeff, fitdist, deriv=False):
    """Polynomial interpolation to the luminosity distance.
    Initial conditions d_L(0)=0 and d'_L(0)=1 are implemented.
    A fourth order order polynomial is computed:
    d_L(z) = z + c_2*z**2 + c_3*z**3 + c_4*z**4 + c_5*z**5

    Parameters
    ----------
    z : float or (n) list
        Redshift(s).
    coeff: list
        Coefficients of the polynomial.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)
    deriv : bool
        If False, return only the distance. If True, return also 1st and
        2nd derivative.

    Returns
    -------
    result : (n,) list or (n,n,n) list
        Luminosity distance dL, optionally including the 1st and 2nd
        derivatives (dL,d1L,d2L).
    """

    dfactor = {'luminosity':0, 'angular':2, 'comoving':1}

    if fitdist not in dfactor:
        raise ValueError("fitdist must be set to one of these options:\n%s"
                         % str(dfactor.keys()))

    c2, c3, c4, c5 = get_coeff(coeff)

    dist = (z + c2*np.power(z,2.) + c3*np.power(z,3)
            + c4*np.power(z,4) + c5*np.power(z,5))
    conv = dfactor[fitdist]
    dL = dist * (1+z)**conv

    if deriv is False:
        result = (dL,)
    else:
        d1L = (conv*(z+1)**(conv-1)*(c5*z**5+c4*z**4+c3*z**3+c2*z**2+z)
               +(z+1)**conv*(5*c5*z**4+4*c4*z**3+3*c3*z**2+2*c2*z+1))
        d2L = ((conv-1)*conv*(z+1)**(conv-2)*(c5*z**5+c4*z**4+c3*z**3+c2*z**2+z)
               +2*conv*(z+1)**(conv-1)*(5*c5*z**4+4*c4*z**3+3*c3*z**2+2*c2*z+1)
               +(z+1)**conv*(20*c5*z**3+12*c4*z**2+6*c3*z+2*c2))
        result = (dL,d1L,d2L)

    return result


def hubble_poly(z, coeff):
    """Polynomial interpolation to the Hubble parameter h=H/H0.
    The initial condition h(0)=1 is implemented, so that:
    h(z) = 1 + c_1*z + c_2*z**2 + c_3*z**3 + ...

    Parameters
    ----------
    z : array_like
        Redshift(s).
    coeff: list
        Coefficients of the polynomial, starting from the order 1 term
        and for increasing order.

    Returns
    -------
    result : array_like
        Hubble parameter h=H/H0 at redshift(s) z.
    """

    hubble = 1.
    for i, ci in enumerate(coeff):
        hubble += ci*z**(i+1.)

    return hubble


def hubble_const_kH(z, dL, d1L, kH):
    """Compute the Hubble parameter given a constant curvature parameter
    kH, and the luminosity distance.

    Equation: h = sqrt((1 - kH*d^2) / d'^2).

    .. note ::
        This functions is provided for convenience, but it is not
        strictly related to polynomial fits.

    Parameters
    ----------
    z : array_like
        Redshift(s).
    dL : array_like
        Luminosity distance at redshifts z.
    d1L : array_like
        First derivative (w.r.t. redshift) of the luminosity distance
        at redshifts z.
    kH : float
        Curvature parameter.

    Return
    ------
    h : float or None
        Return the hubble parameter h, or None if h^2<0.

    """
    d_co = dL/(1.+z)
    d1_co = d1L/(1.+z) - np.power(1.+z,-2.)*dL

    hnum = 1. - kH * d_co**2.
    if np.any(hnum < 0.):
        return None
    else:
        return np.sqrt(hnum / d1_co**2.)


def sachs_integrand(z, coeff, fitdist):
    dL = dL_poly(z, coeff, fitdist)[0]
    return np.power( dL, 2)


def sachs_integral(z, coeff, fitdist):
    """Return integrate(dL**2, 0, z).

    .. note ::
        The analytical results employed here have been automatically
        generated integrating polynomials with Maxima. If they need to
        be modified, Sympy could be used to produce such expressions
        at run time.
    """

    c2, c3, c4, c5 = get_coeff(coeff)

    if fitdist == 'luminosity':
        result = (1260*c5**2*z**11+2772*c4*c5*z**10
                  +(3080*c3*c5+1540*c4**2)*z**9
                  +(3465*c2*c5+3465*c3*c4)*z**8
                  +(3960*c5+3960*c2*c4+1980*c3**2)*z**7
                  +(4620*c4+4620*c2*c3)*z**6
                  +(5544*c3+2772*c2**2)*z**5
                  +6930*c2*z**4+4620*z**3)/13860
    elif fitdist == 'comoving':
        result =  (13860*c5**2*z**13+(30030*c5**2+30030*c4*c5)*z**12
                   +(16380*c5**2+(65520*c4+32760*c3)*c5+16380*c4**2)*z**11
                   +((36036*c4+72072*c3+36036*c2)*c5+36036*c4**2
                     +36036*c3*c4)*z**10
                   +((40040*c3+80080*c2+40040)*c5+20020*c4**2
                     +(80080*c3+40040*c2)*c4+20020*c3**2)*z**9
                   +((45045*c2+90090)*c5+(45045*c3+90090*c2+45045)*c4
                     +45045*c3**2+45045*c2*c3)*z**8
                   +(51480*c5+(51480*c2+102960)*c4+25740*c3**2
                     +(102960*c2+51480)*c3+25740*c2**2)*z**7
                   +(60060*c4+(60060*c2+120120)*c3+60060*c2**2+60060*c2)*z**6
                   +(72072*c3+36036*c2**2+144144*c2+36036)*z**5
                   +(90090*c2+90090)*z**4+60060*z**3)/180180
    elif fitdist == 'angular':
        result = (12012*c5**2*z**15+(51480*c5**2+25740*c4*c5)*z**14
                  +(83160*c5**2+(110880*c4+27720*c3)*c5+13860*c4**2)*z**13
                  +(60060*c5**2+(180180*c4+120120*c3+30030*c2)*c5+60060*c4**2
                    +30030*c3*c4)*z**12
                  +(16380*c5**2+(131040*c4+196560*c3+131040*c2+32760)*c5
                    +98280*c4**2+(131040*c3+32760*c2)*c4+16380*c3**2)*z**11
                  +((36036*c4+144144*c3+216216*c2+144144)*c5+72072*c4**2
                    +(216216*c3+144144*c2+36036)*c4+72072*c3**2
                    +36036*c2*c3)*z**10
                  +((40040*c3+160160*c2+240240)*c5+20020*c4**2
                    +(160160*c3+240240*c2+160160)*c4+120120*c3**2
                    +(160160*c2+40040)*c3+20020*c2**2)*z**9
                  +((45045*c2+180180)*c5+(45045*c3+180180*c2+270270)*c4
                    +90090*c3**2+(270270*c2+180180)*c3+90090*c2**2
                    +45045*c2)*z**8
                  +(51480*c5+(51480*c2+205920)*c4+25740*c3**2
                    +(205920*c2+308880)*c3+154440*c2**2+205920*c2+25740)*z**7
                  +(60060*c4+(60060*c2+240240)*c3+120120*c2**2+360360*c2
                    +120120)*z**6
                  +(72072*c3+36036*c2**2+288288*c2+216216)*z**5
                  +(90090*c2+180180)*z**4+60060*z**3)/180180
    return result


def sachs_h2_num(z, coeff, Omega_m0, fitdist):
    """Return the squared numerator of the Sachs equation."""

    dL = dL_poly(z, coeff, fitdist)[0]
    d_co  = dL/(1.+z)

    integral = sachs_integral(z, coeff, fitdist)

    h2_num = 1. \
             - 3./2.*Omega_m0*np.power((1.+z),3.)\
             *np.power(d_co,2) \
             + 15./2.*Omega_m0*integral

    return h2_num


def sachs_h_den_pm(z,coeff, fitdist):
    """Return the denominator of the Sachs equation, up to a sign."""

    dL, d1L, d2L = dL_poly(z, coeff, fitdist, deriv=True)
    d_co  = dL/(1.+z)
    d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

    h_den = (1.+z)*d1_co-d_co

    return h_den


def sachs_z0(coeff, limits, fitdist):
    """Compute the redshift z0 at the angular diameter distance maximum
    d'_A(z0)=0.

    Parameters
    ----------
    coeff: list
        Polynomial coefficients.
    limits : (float, float)
        Redshift limits where to look for a maximum.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Returns
    -------
    z0 : float or None


    TODO
    ----
        Allow the angular diameter distance to have no maximum.
    """

    c2, c3, c4, c5 = get_coeff(coeff)
    zmin, zmax = limits

    try :
        z0 = optimize.bisect(sachs_h_den_pm, zmin, zmax,
                             args=((c2,c3,c4,c5),fitdist))
        return z0

    except ValueError:
        return None


def sachs_Omega_m(z0, coeff, fitdist):
    """Set Omega_m0 such that the numerator of the Hubble parameter is
    zero at the redshift z0 where the angular diameter distance is
    maximal.

    Parameters
    ----------
    z0 : redshift z0 at the angular diameter distance maximum
        d'_A(z0)=0
    coeff: list
        Polynomial coefficients.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Returns
    -------
    Omega_m0 : float

    TODO
    ----
        Allow the angular diameter distance to have no maximum.
    """

    dL = dL_poly(z0, coeff, fitdist)[0]
    d_co  = dL/(1.+z0)
    integral = sachs_integral(z0, coeff, fitdist)

    Omega_m0 = 1. / (3./2.*np.power((1.+z0),3.)*np.power(d_co,2)\
                     - 15./2.*integral)

    return Omega_m0


def sachs_hubble(z, coeff, Omega_m0, fitdist):
    """Return the Hubble parameter consistent with a backreaction model."""
    h_num = np.sqrt(sachs_h2_num(z, coeff, Omega_m0, fitdist))
    h_den = np.abs(sachs_h_den_pm(z,coeff, fitdist))

    hubble = h_num / h_den

    return hubble


def flatFRW_hubble(z, coeff, fitdist):
    """Return the Hubble parameter consistent with a flat FRW metric."""

    dL, d1L, d2L = dL_poly(z, coeff, fitdist, deriv=True)
    d_co  = dL/(1.+z)
    d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

    return 1./d1_co


def validate_dL(z, coeff, fitdist):
    """Add penalization to the likelihood if angular diameter
    distance is negative and if it has more than one maximum.

    Parameters
    ----------
    z : array
        Array of redshifts.
    coeff : list
        Polynomial coefficients.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Return
    ------
    valid : bool
        True if the angular diameter distance is positive and has at
        most one maximum, False otherwise.
    """

    validate = True

    dL = dL_poly(z, coeff, fitdist)[0]

    # dL must be positive
    if np.any(dL < 0):
        validate = False

    # dA can have at most one extreme (maximum)
    #
    # sort the data in x and use that to rearrange y
    if validate is True:
        sortId = np.argsort(z)
        zsort  = z[sortId]
        dLsort = dL[sortId]
        dA = dLsort / np.power(1.+zsort,2)
        imax = signal.argrelmax(dA)[0]
        imin = signal.argrelmin(dA)[0]
        Nextrm = len(imax) + len(imin)
        if Nextrm > 1:
            validate = False

    # No sign change in dA''. If the validity of the Hubble parameter
    # is also required, this is redundant (still, it provides a
    # consistency test), since these curves are anyway excluded when
    # the following is required: dA'=0 once and only once, h^2>0.
    check_dd_dA = False
    if validate and check_dd_dA:
        from scipy import interpolate
        from scipy.misc import derivative
        dd_dA = np.asarray([])
        f = interpolate.interp1d(z,dA)
        for i, zi in enumerate(z):
            if i>0 and i<len(z)-1:
                dd_dA = np.append(dd_dA,
                                  derivative(f,zi,dx=1e-6,n=2))
                pos = np.any(dd_dA>0)
                neg = np.any(dd_dA<0)
        if pos and neg:
            validate = False

    return validate


def validate_sachs(z, coeff, fitdist):
    """The Sachs equation involve the square root of the quantity here
    computed. If the latter a negative, return a False validation.

    Parameters
    ----------
    z : array
        Array of redshifts.
    coeff : list
        Polynomial coefficients.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Return
    ------
    valid : bool
            True if the Sachs equation can be computed, False otherwise.
    """
    valid = True

    dL, d1L, d2L = dL_poly(z, coeff, fitdist,
                           deriv=True)

    # Comoving distance
    d_co  = dL/(1.+z)
    d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

    z0 = sachs_z0(coeff, (z[0],z[-1]), fitdist)
    if z0:
        Omega_m0 = sachs_Omega_m(z0, coeff, fitdist)
        h2_num = sachs_h2_num(z, coeff, Omega_m0,
                              fitdist)

        # If the angular diameter distance dA has at most one
        # maximum, and if Omega_m0 can be computed (which in this
        # case corresponds to requiring that dA has one maximum),
        # then the square of the Hubble parameter numerator must
        # necessarily be positive. Unless dA changes curvature at
        # a z (without maxima). In this case h2_num can still be
        # negative.
        if np.any(h2_num < 0):
            valid = False
    else:
        valid = False

    return valid


def Omega_m0_sachs_rand(z, coeff, fitdist):
    """Retrieve Omega_m0 by requiring a finite Hubble parameter. If the
    angular diameter distance has no maximum, return a random value
    Omega_m0>=0, bounded from the top by requiring a positive square
    numerator of the Hubble parameter and Omega_m0<1.

    Parameters
    ----------
    c2, c3, c4, c5: floats
        Coefficients of the polynomial.

    Return
    ------
    Omega_m0 : float or None
        Return Omega_m0 value, or None.

    """

    maxiter = 100

    dL, d1L, d2L = dL_poly(z, coeff, fitdist,
                           deriv=True)

    # Comoving distance
    d_co  = dL/(1.+z)
    d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

    z0 = sachs_z0(coeff, (z[0],z[-1]), fitdist)

    if z0:
        Omega_m0 = sachs_Omega_m(z0, coeff, fitdist)
        h2_num = sachs_h2_num(z, coeff, Omega_m0,
                              fitdist)

        if np.any(h2_num < 0):
            Omega_m0 = None
    else:
        rand = np.random.rand(maxiter)
        for Omm in rand:
            h2_num = sachs_h2_num(z, coeff, Omm,
                                  fitdist)
            if np.all(h2_num > 0):
                Omega_m0 = Omm
                break
            else:
                Omega_m0 = None

    return Omega_m0


def get_wDtot(redshifts, dL, d1L, d2L):
    """Return the equation of state: Eq. 3.13 of Boehm and Rasanen
    arXiv:1305.7139, where h=1/d' (flat FLRW).

    Parameters
    ----------
    redshifts : array_like
        Redshift list.
    dL : array_like
        Luminosity distance at z's.
    d1L : array_like
        First derivative of luminosity distance at z's.
    d2L : array_like
        Second derivative of luminosity distance at z's.

    """

    zp1 = redshifts + 1.
    diff = zp1*d1L - dL

    # Compute wDtot = 2/3*(1+z)*h'/h-1, where h=1/d' (flat FLRW).
    wDtot = - 2./3. * zp1**2.*d2L / diff + 1./3.

    return wDtot


def get_wHtot(redshifts, Omega_m0, dL, d1L, d2L, hubble):
    """Return the equation of state: Eq. 3.13 of Boehm and Rasanen
    arXiv:1305.7139, where h is given by Sachs optical equation
    (backreaction case).

    Parameters
    ----------
    redshifts : array_like
        Redshift list.
    Omega_m0 : flt
        Matter density parameter today.
    dL : array_like
        Luminosity distance at z's.
    d1L : array_like
        First derivative of luminosity distance at z's.
    d2L : array_like
        Second derivative of luminosity distance at z's.
    hubble : array_like
        Hubble parameter at z's.

    """

    zp1 = redshifts+1.
    Omm0 = Omega_m0

    # Compute wHtot = 2/3*(1+z)*h'/h-1, where h is given by Sachs
    # optical equation. The following expression has been derived
    # using h'/h = 1/(2h^2) * d(h^2)/dz , where d(h^2)/dz is easily
    # computed.

    num = 2.*(-zp1**2*d2L + 2.*(zp1*d1L-dL)
              - (3.*Omm0*zp1**3*dL)/(2.*hubble**2))
    den = 3.*(zp1*d1L - 2.*dL)
    wHtot = num/den - 1.

    ## The following is a more pedantic form, for test purposes only.
    # dA = dL / zp1**2.
    # d1A = d1L / zp1**2. - 2. * dL / zp1**3.
    # d2A = d2L / zp1**2. - 4. * d1L / zp1**3. + 6. * dL / zp1**4.
    # hprime_over_h = - ((2. * d1A / zp1 + d2A + 1.5 * Omm0 * zp1 * dA /
    #                     hubble**2.)  / d1A)
    # wHtot = 2./3.* zp1 * hprime_over_h - 1.

    return wHtot


def get_wD(redshifts, Omega_m0, dL, d1L, d2L):
    """Return the equation of state: Eq. 3.12 of Boehm and Rasanen
    arXiv:1305.7139, where h=1/d' (flat FLRW).

    Parameters
    ----------
    redshifts : array_like
        Redshift list.
    Omega_m0 : flt
        Matter density parameter today.
    dL : array_like
        Luminosity distance at z's.
    d1L : array_like
        First derivative of luminosity distance at z's.
    d2L : array_like
        Second derivative of luminosity distance at z's.

    """

    zp1 = redshifts+1.
    Omm0 = Omega_m0
    d1 = d1L/zp1 - dL/zp1**2.

    # Compute wD = (2/3*(1+z)*h'/h-1) / (1-Omm), where Omm(z) =
    # Omm0*(1+z)^3/h(z)^2 and h=1/d' (flat FLRW).
    wD = get_wDtot(redshifts, dL, d1L, d2L) / (1. - Omm0 * zp1**3. * d1**2.)

    return wD


def get_wH(redshifts, Omega_m0, dL, d1L, d2L, hubble):
    """ Return the equation of state: Eq. 3.12 of Boehm and
    Rasanen arXiv:1305.7139, where h is given by Sachs optical
    equation (backreaction case).

    Parameters
    ----------
    redshifts : array_like
        Redshift list.
    Omega_m0 : flt
        Matter density parameter today.
    dL : array_like
        Luminosity distance at z's.
    d1L : array_like
        First derivative of luminosity distance at z's.
    d2L : array_like
        Second derivative of luminosity distance at z's.
    hubble : array_like
        Hubble parameter at z's.
    """

    zp1 = redshifts+1.
    Omm0 = Omega_m0

    # Compute wH = (2/3*(1+z)*h'/h-1) / (1-Omm), where h is given by
    # Sachs optical equation and Omm(z) = Omm0*(1+z)^3/h(z)^2.
    wH = (get_wHtot(redshifts, Omega_m0, dL, d1L, d2L, hubble)
          / (1. - Omm0 * zp1**3. / hubble**2.))

    return wH


def _integrandls(z, coeff, Omega_m0, fitdist):
    hubble = sachs_hubble(z, coeff, Omega_m0, fitdist)
    d = dL_poly(z, coeff, fitdist, deriv=False)[0] / (1.+z)
    return 1. / (d**2. * hubble)


def _get_intls(zl, zslist, coeff, Omega_m0, fitdist):
    """Compute the integral entering in d(z_l,z_s)."""
    intls = []
    for zs in zslist:
        res, __ = integrate.quad(_integrandls, zl, zs,
                                 args=(coeff, Omega_m0, fitdist))
        intls += [res]
    return np.asarray(intls)


def sachs_sumrule_d(zl, zs_array, coeff, Omega_m0, fitdist):
    """Compute distances entering in the sum rule, eq. 5 of
    arxiv:1412.4976.

    Parameters
    ----------
    zl: float
        Redshift of the lens.
    zs_array: array_like
        Redshift(s) of the source(s).
    coeff: list
        Coefficients of the polynomial interpolation to the distance.
    Omega_m0: float
        Matter density parameter today.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    """
    dl = (dL_poly(zl, coeff, fitdist, deriv=False)[0] / (1.+zl))
    ds_array = (dL_poly(zs_array, coeff, fitdist, deriv=False)[0]
                / (1.+zs_array))

    intls_array = _get_intls(zl, zs_array, coeff, Omega_m0, fitdist)
    dls_array = dl*ds_array*intls_array

    return (dl, ds_array, dls_array)


def kS_sachs(dl, ds_array, dls_array):
    """Compute the angular diameter distance sum rule.

    Parameters
    ----------
    dl: float
        Proper distance to the lens.
    ds_array: array_like
        Proper distance(s) to the source.
    dls_array: array_like
        Proper separation(s) between the lens and source(s).

    Returns
    -------
    kS : array_like
        Distance(s) sum rule.
    """
    dl2 = dl**2.
    ds2 = ds_array**2.
    dls2 = dls_array**2.

    kS = (dl2**2. + ds2**2. + dls2**2.  - 2.*dl2*ds2 - 2.*dl2*dls2
          - 2.*ds2*dls2)
    kS /= -(4.*dl2*ds2*dls2)

    return kS


def kP_sachs(zlist, coeff, Omega_m0, fitdist):
    """Compute the parallax distance sum rule.

    To compute kP use the fact that kP(z)=kS(z,z). To avoid
    divergences, compute kS(z, z+eps) for a small (but not too small)
    eps.

    Parameters
    ----------
    zlist: array_like
        Redshifts.
    coeff: list
        Coefficients of the polynomial interpolation to the distance.
    Omega_m0: float
        Matter density parameter today.
    fitdist : str
        Specify which distance is fitted:
        - 'luminosity' (default) dL
        - 'angular' dA = dL / (1+z)**2
        - 'comoving'  d = dL / (1+z)

    Returns
    -------
    kP : array_like
        Parallax distance sum rule.

    """

    eps = 1e-5
    kP = np.empty(len(zlist))

    for i, zl in enumerate(zlist):
        dl, ds_array, dls_array = sachs_sumrule_d(zl,
                                                  np.array([zl+eps]),
                                                  coeff,
                                                  Omega_m0,
                                                  fitdist)
        kP[i] = kS_sachs(dl, ds_array, dls_array)[0]

    return kP
