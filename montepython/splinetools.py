"""
.. module:: splinetools
   :synopsis: Simple cubic spline.
.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

This module defines functions for a simple cubic spline interpolation.

"""

import bisect
import numpy as np
import scipy.linalg as la

def splined (x, y, deriv_0=(0.,2), deriv_f=(0.,2), want_y1=False):
    """Returns an array containing the spline second derivatives.
    Optionally, both the spline first and second derivatives can be
    returned.  It solves a system h * y'' = v by tridiagonal
    decomposition.  It can be used to provide the spline second
    derivatives as input to :func:`spline()`, in which case it has to
    be called only once for any desired interpolated point.

    Boundary conditions can be specified by setting the value of the
    first or second derivatives at the initial and final abscissa. If
    no condition is specified, natural splines are assumed (zero
    second derivative).

    Parameters
    ----------
    x : array
        Array sorted in ascending order containing the abscissa of the
        tabulated function.
    y : array
        It contains the tabulated function y_i=y(x_i).
    deriv_0 : (float, int)
        Optional. Specifies the derivative (value, order), where order
        is 1 or 2, for the boundary condition at the initial abscissa.
    deriv_f : (float, int)
        Optional. Specifies the derivative (value, order), where order
        is 1 or 2, for the boundary condition at the final abscissa.
    want_y1 : bool
        Optional. Set to True to return the array of first derivatives
        as output.

    Returns
    -------
    result : (n,) or (n,n) array.
        Second derivative, or first and second derivative arrays,
        computed according to the cubic spline algorithm. Here n
        is the dimension of the input arrays.

    Examples
    --------
    >>> import numpy as np
    >>> import splinetools as st

    >>> x = [0.,0.1,0.3,0.65,.83]
    >>> y = np.sin(x)

    >>> y1ex = np.cos(x)
    >>> y2ex = -np.sin(x)

    >>> y2_0 = -np.sin(x[0])
    >>> y1_f = np.cos(x[-1])
    >>> y1,y2 = st.splined(x, y, deriv_0=(y2_0,2), deriv_f=(y1_f,1),
                            want_y1=True)

    >>> np.set_printoptions(precision=5)

    >>> print "y' exact:", y1ex

    y' exact: [ 1.       0.995    0.95534  0.79608  0.67488]

    >>> print "y' spline:", y1

    y' spline: [ 0.99999  0.99502  0.95519  0.7962   0.67488]

    >>> print "y'' exact:", y2ex

    y'' exact: [-0.      -0.09983 -0.29552 -0.60519 -0.73793]

    >>> print "y'' spline:", y2

    y'' spline: [ 0.      -0.09936 -0.29891 -0.60961 -0.73847]

    """

    if (len(deriv_0) != 2) or (len(deriv_f) != 2):
        raise ValueError('Boundary conditions format must be '
                          '(value,order), containing the value of the '
                          'derivative and its order (1 or 2 for first '
                          'or second derivative)')

    if np.all(np.diff(x) > 0)==False :
        raise ValueError('The input x array must be sorted in strictly '
                         'ascending order!')
    if len(x) != len(y) :
        raise ValueError('The x and y arrays must have the same size!')

    if deriv_0[1] is 1:
        y1_0 = deriv_0[0]
        y2_0 = None
    elif deriv_0[1] is 2:
        y1_0 = None
        y2_0 = deriv_0[0]
    else:
        raise ValueError('The order of the derivative '
                         'deriv_0=(value,order) must be an integer '
                         'equal to 1 or 2')

    if deriv_f[1] is 1:
        y1_f = deriv_f[0]
        y2_f = None
    elif deriv_f[1] is 2:
        y1_f = None
        y2_f = deriv_f[0]
    else:
        raise ValueError('The order of the derivative '
                         'deriv_f=(value,order) must be an integer '
                         'equal to 1 or 2')

    # if ((y1_0 is None) and (y2_0 is None))\
    #    or ((y1_0 is not None) and (y2_0 is not None)):
    #     print "ERROR in splined(): Boundary conditions y1_0 and y2_0 cannot be"
    #     print "set both to None or to scalar!"
    #     exit()
    # if ((y1_f is None) and (y2_f is None))\
    #    or ((y1_f is not None) and (y2_f is not None)):
    #     print "ERROR in splined(): Boundary conditions y1_f and y2_f cannot be"
    #     print "set both to None or to scalar!"
    #     exit()

    n = len(y)
    low = 1
    up = 1

    a  = np.zeros((n, n))
    ab = np.zeros((low+up+1, n))
    #ab = np.zeros((up+1, n))
    b  = np.zeros(n)

    # Fill in depending on boundary conditions.
    # It is enough to fill the upper diagonal and the diagonal,
    # since the matrix is symmetric.
    if y1_0 is None:
        a[0,0] = 1.
        b[0]   = y2_0
    else:
        a[0,0] = (x[1]-x[0]) / 3.
        a[0,1] = (x[1]-x[0]) / 6.
        b[0]   = (y[1]-y[0]) / (x[1]-x[0]) - y1_0

    if y1_f is None:
        a[n-1,n-1] = 1.
        b[n-1]     = y2_f
    else:
        a[n-1,n-1] = (x[n-1]-x[n-2]) / 3.
        a[n-1,n-2] = (x[n-1]-x[n-2]) / 6.
        b[n-1]     = y1_f - (y[n-1]-y[n-2]) / (x[n-1]-x[n-2])

    for i in range(1,n-1):
        a[i,i-1] = (x[i]-x[i-1]) / 6.
        a[i,i]   = (x[i+1]-x[i-1]) / 3.
        a[i,i+1] = (x[i+1]-x[i]) / 6.
        b[i]     = (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1])

    # We have to write the diagonals as arrays, starting from to upper
    # diagonal. Fill in the upper diagonal and the central diagonal.
    for i in range(up+1):
        k = 0
        for j in range(up-i,n):
            ab[i,j] = a[k,j]
            k += 1
    # Fill in the lower diagonal.
    k = 1
    for i in range(up+1,low+up+1):
        for j in range(n-k):
            ab[i,j] = a[k,j]
            k += 1

    # Solve system h * y'' = v by tridiagonal decomposition
    y2 = la.solve_banded((low, up), ab, b)

    # # Matrix whose lines is composed by the upper diagonal and the diagonal
    # for i in range(1,n):
    #     ab[0,i] = a[i-1,i]
    # ab[1,:] = np.diag(a)

    # # Solve system h * y'' = v by tridiagonal decomposition
    # y2 = la.solveh_banded(ab, b)

    if want_y1 is True:
        y1 = np.zeros(n)
        if y1_0 is None:
            y1[0] = (y[1]-y[0]) / (x[1]-x[0]) - (x[1]-x[0]) * y2[0] / 3.\
                    - (x[1]-x[0]) * y2[1] / 6.
        else:
            y1[0] = y1_0
        if y1_f is None:
            y1[n-1] = (y[n-1]-y[n-2]) / (x[n-1]-x[n-2])\
                      + (x[n-1]-x[n-2]) * y2[n-2] / 6.\
                      + (x[n-1]-x[n-2]) * y2[n-1] / 3.
        else:
            y1[n-1] = y1_f
        for ii in range(1,n-1):
            y1[ii] = (y[ii+1]-y[ii]) / (x[ii+1]-x[ii])\
                     - (x[ii+1]-x[ii]) * y2[ii] / 3.\
                     - (x[ii+1]-x[ii]) * y2[ii+1] / 6.
        result = (y1,y2)
    else:
        result = y2

    return result


def spline(x, y, y2, xint):
    """
    Returns a cubic-spline interpolated value at a specified abscissa
    x, given a tabulated function and its second derivatives array.

    Parameters
    ----------
    x : array
        Tabulated abscissa array.
    y : array
        Tabulated function array, y_i = y(x_i).
    y2 : array
        Second derivatives of the interpolated function, it can be
        computed with :func:`splined()`.
    xint : float
        Abscissa at the desired interpolated value.

    Returns
    -------
    y : float.
        Cubic spline value at the point xint.

    Examples
    -------
    >>> import numpy as np
    >>> import splinetools as st

    >>> x = [0, 0.5, 1.0, 1.5, 2.0]
    >>> y = np.exp(x)
    >>> yin = np.exp(x[0])
    >>> yfi = np.exp(x[-1])

    >>> y2 = st.splined(x, y, deriv_0=(yin, 2), deriv_f=(yfi, 2))

    >>> xint = 1.7
    >>> print("Exact: y = %g"%np.exp(xint))

    Exact: y = 5.47395

    >>> print("Spline: y = %g"%st.spline(x, y, y2, xint))

    Spline: y = 5.47495

    """

    if np.all(np.diff(x) > 0)==False :
        raise ValueError('ERROR in spline(): the input x array must be '
                         'sorted in strictly ascending order')
    if len(x) != len(y) :
        raise ValueError('ERROR in spline(): the x and y arrays must '
                         'have the same size')

    if (xint < x[0]) or (xint > x[-1]):
        raise IndexError('list index out of range')

    n = len(y)

    if xint == x[0]:
        ii = 0
    elif xint == x[n-1]:
        ii = n-2
    else:
        #Find rightmost value less than or equal to xint
        ii = bisect.bisect_right(x, xint)-1

    h = x[ii+1] - x[ii]
    if (h==0.):
        raise ValueError('The x elements must be distinct')

    A = (x[ii+1]-xint) / h
    B = 1. - A
    C = D = pow(h,2.) / 6.0
    C *= (pow(A,3.)-A)
    D *= (pow(B,3.)-B)

    # Cubic spline polynomial is now evaluated.
    y = A*y[ii] + B*y[ii+1] + C*y2[ii] + D*y2[ii+1]

    return y


def spline_p0(z, x, y, y_0, deriv_0=(0., 2), deriv_f=(0., 2),
              want_y1=False):
    """Compute a spline for a data array with knots `x[i] > 0`.  It adds a
    point `x_0 = 0.0` so that an initial condition for `y_0` can be
    given, together with the derivatives at the initial (`x_0`) and
    final points. The input interpolating point(s) z can be an array.

    Parameters
    ----------
    z : (n) array
        Interpolating points.
    x : (m) array
        Array sorted in ascending order containing the abscissa of the
        tabulated function (knots of the spline). Elements must be x_i
        > 0.
    y : (m) array
        It contains the tabulated function y_i=y(x_i).
    y_0 : float
        Initial condition y(x=0).
    deriv_0 : (float, int)
        Optional. Specifies the derivative (value, order), where order
        is 1 or 2, for the boundary condition at the initial abscissa.
    deriv_f : (float, int)
        Optional. Specifies the derivative (value, order), where order
        is 1 or 2, for the boundary condition at the final abscissa.

    Returns
    -------
    Two or three element list with the following elements.

    spline0 : (n) array.
        Spline function at the input interpolating points.
    y1 : (m) array
        Optional, only computed if want_y1=True. First derivatives at
        spline knots.
    y2 : (m) array
        Second derivatives at spline knots.

    """

    if np.any(x <= 0.):
        raise ValueError('The input array of interpolating points '
                          'contains a negative element, which is not '
                          'consistent with the scope of this '
                          'function.')

    # Spline points to be fitted
    y = np.array(y)
    x = np.array(x)

    # Add point x0=0, y0=0 ad the beginning of the arrays to implement
    # initial conditions
    x_0 = 0.
    x = np.insert(x, 0, x_0)
    y = np.insert(y, 0, y_0)

    if want_y1 is True:
        y1, y2 = splined(x, y, deriv_0, deriv_f, want_y1)
    else:
        y2 = splined(x, y, deriv_0, deriv_f)

    spline0 = np.zeros((len(z)), 'float64')
    for i in range(len(spline0)):
        spline0[i] = spline(x, y, y2, z[i])

    if want_y1 is True:
        ret_list = (spline0, y1[1:], y2[1:])
    else:
        ret_list = (spline0, y2[1:])

    return ret_list
