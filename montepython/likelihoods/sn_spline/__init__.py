import os
import numpy as np
from montepython.likelihood_class import Likelihood
import montepython.scspline as scs
import operator
from scipy import integrate
from scipy import interpolate
import re
import manipulate_strings as mstr
from scipy.signal import argrelmax

class sn_spline(Likelihood):

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        data_path = os.path.join(self.data_directory, self.z_mu_dmu)

        # define array for values of z and data points
        self.z = np.array([], 'float64')
        self.moduli = np.array([], 'float64')

        # read redshifts and data points
        for line in open(data_path, 'r'):
            if (line.find('#') == -1):
                self.z = np.append(self.z, float(line.split()[1]))
                self.moduli = np.append(self.moduli, float(line.split()[2]))

        # number of data points
        self.num_points = np.shape(self.z)[0]

        # define correlation matrix
        covmat = np.zeros((self.num_points, self.num_points), 'float64')

        # file containing correlation matrix
        if self.has_syscovmat:
            covmat_filename = self.covmat_sys
        else:
            covmat_filename = self.covmat_nosys

        # read correlation matrix
        i = 0
        for line in open(os.path.join(
                self.data_directory, covmat_filename), 'r'):
            if (line.find('#') == -1):
                covmat[i] = line.split()
                i += 1

        # invert correlation matrix
        self.inv_covmat = np.linalg.inv(covmat)

        # find sum of all matrix elements (sounds odd that there is
        # not a trace here instead, but this is correct!)
        self.inv_covmat_sum = np.sum(self.inv_covmat)


        if self.dictionary['knots'] == 'auto':
            log_path = os.path.join(command_line.folder, 'log.param')
            self.set_knots(log_path)


    def set_knots(self,log_path):
        """Set the spline knots on a logarithmic scale.

        Parameters
        ----------
        log_path : string
                   path of the log.param file
        """
        # Sort z array in ascending order and read initial/final redshifts
        L = sorted(zip(self.z,self.moduli), key=operator.itemgetter(0))
        z_sorted, mu_sorted = np.asarray(zip(*L))
        self.logz_start=np.log10(z_sorted[0])
        self.logz_stop=np.log10(z_sorted[-1])

        # Define increasing spline knots on log scale.
        self.knots=np.logspace(self.logz_start, self.logz_stop,\
                               num=len(self.use_nuisance),base=10.0)

        # Write the knots to the .data file to record correctly log.param.
        karrstr = np.char.mod('%f', self.knots) # String array
        kstr = "["+", ".join(karrstr)+"]\n" # Combine to a string
        klabel = self.name+'.knots'
        mstr.replace(log_path,\
                     '(?m)^'+re.escape(klabel)+'.*\n?',klabel+'='+kstr)

        # Update also the dictionary
        self.dictionary['knots'] = self.knots


    def loglkl(self, cosmo, data):

        # Technically all parameters varying in the likelihood, but not in the Boltzmann code, are all nuisance parameters
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])
        # fill with nuisance parameters
        for nuisance in nuisance_parameter_names:
            if nuisance == 'c1':
                c1 = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c2':
                c2 = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c3':
                c3 = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c4':
                c4 = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c5':
                c5 = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']

        # Spline points to be fitted
        y = np.array([c1,c2,c3,c4,c5])
        x = self.knots

        self.y_0=0 # Initial condition dL(z=0)=0
        self.y1_0=1. # Initial condition dL'(z=0)=1

        d_spline, y2 = scs.spline_p0(self.z,x,y,self.y_0,deriv_0=(self.y1_0,1))

        if self.validate_dL(self.z,d_spline) is False:
            d_spline = [1.e30]*len(d_spline)
            d_spline = np.asarray(d_spline)

        # Hubble parameter must be h(z) > 0
        if self.sachs_condition is True:
            if self.validate_sachs(self.knots,y) is False:
                d_spline = [1.e30]*len(d_spline)
                d_spline = np.asarray(d_spline)


        # define array for difference between theory and observations
        difference = np.ndarray(self.num_points, 'float64')

        # for each point, compute luminosity distance d_L=(1+z)**2d_A and infer
        # theoretical prediction and difference with observation
        for i in range(self.num_points):
            difference[i] = 5 * np.log10(d_spline[i]) + self.M - self.moduli[i]

        # chisquare before analytic marginalization
        AT = np.dot(difference, np.dot(self.inv_covmat, difference))

        # Add roughness parameter. Remember that cubic splines have linear
        # second derivatives.
        if self.rough_lambda < 0:
            print "ERROR, roughness parameter must be non-negative"
            exit(1)
        elif self.rough_lambda == 0:
            rough = 0.
        else:
            self.d_prime_prime = interpolate.interp1d(x, y2, kind='slinear')

            result,err = integrate.quad(self.rough_integrand, x[1], x[-1])
            rough = self.rough_lambda*result

            # import pylab as pl
            # L = sorted(zip(self.z,self.moduli), key=operator.itemgetter(0))
            # z_sorted, mu_sorted = np.asarray(zip(*L))
            # fig = pl.figure()
            # ax = fig.add_subplot(2,1,1)
            # ax.plot(z_sorted, self.d_prime_prime(z_sorted))
            # ax.plot(x, self.d_prime_prime(x))
            # ax.plot(x, np.power(self.d_prime_prime(x)/x,2))
            # # ax.set_xscale('log')
            # # ax.set_yscale('log')
            # pl.show()
            # print rough
            # exit()

        # final chi square
        chi_squared = AT + rough

        # return ln(L)
        lkl = - 0.5 * chi_squared

        return lkl


    def rough_integrand(self,z):
        """Integrand to compute the roughness factor"""
        # int (d''(ln z))**2 d ln z, where d ln z = dz/z
        return self.d_prime_prime(z)**2./z


    def validate_dL(self,z,dL):
        """ Add penalization to the likelihood if physical conditions are not
        satisfied.

        Parameters
        ----------
        z : list
            Input redshifts.
        dL : list
            Luminosity distance at the input redshifts

        Return
        ------
        valid : bool
            True if the Sachs equation can be computed, False otherwise.

        -------
        """

        validate = True

        # dL must be positive
        if np.any(dL <= 0):
            validate = False

        # d_A can have at most one extreme (maximum)
        #
        # sort the data in x and use that to rearrange y
        sortId = np.argsort(z)
        zsort  = z[sortId]
        dLsort = dL[sortId]
        d_A = dLsort / np.power(1.+zsort,2)
        imax = argrelmax(d_A)[0]
        Nmax = len(imax)
        if Nmax > 1:
            validate = False

        # Comoving distance must be monotonically increasing
        d_co = dLsort / (1.+zsort)
        if not np.all(d_co==np.sort(d_co)):
            validate = False

        return validate


    def validate_sachs(self,xdata,ydata,Nh=200):
        """ The Sachs equation involve the square root of the quantity here
        computed. If the latter a negative, return a False validation.

        Parameters
        ----------
        xdata : (n) list
            Spline knots.
        ydata : (n) list
            Value of the luminosity distance at the spline knots.
        Nh : int
            Number of redshifts to check for divergences in h(z).

        Return
        ------
        valid : bool
                True if the Sachs equation can be computed, False otherwise.
        """
        valid = True

        self.setXrange(Nh)

        z = self.xlum

        # The fit is done on dL (not d_co).
        dL, y1, y2 = scs.spline_p0(z,xdata,ydata,\
                                   self.y_0,deriv_0=(self.y1_0,1),\
                                   want_y1=True)
        d_co = dL/(1.+z)

        # Add point at zero for correct initial conditions of y1
        z0 = np.insert(xdata,0,0)
        y1 = np.insert(y1,0,self.y1_0)
        # For cubic splines the first derivatives are quadratic
        d1L_func = interpolate.interp1d(z0, y1, kind='quadratic')
        d1L      = d1L_func(z)
        d1_co    = d1L/(1.+z) - np.power(1.+z,-2)*dL

        # Integrate using trapezoidal rule
        z0 = 1.e-30
        Nstep = 50
        integral = np.zeros(len(z))
        for i in range(len(z)):
            xint = np.linspace(z0,z[i],Nstep)
            dL_i = scs.spline_p0(xint,xdata,ydata,\
                               self.y_0,deriv_0=(self.y1_0,1))[0]
            d_co_i = dL_i/(1.+xint)
            yint = np.power( (1+xint) * d_co_i, 2)
            integral[i] = np.trapz(yint, x=xint)

        one_plus_z = 1.+z

        for Omega_m in [0.2,0.25,0.3]:
            sqrtarg = 1. \
                      - 3./2.*Omega_m*np.power(one_plus_z,3.)*np.power(d_co,2) \
                      + 15./2.*Omega_m*integral
            if np.any(sqrtarg < 0):
                valid = False
            else:
                hz = np.sqrt( sqrtarg  ) / np.fabs(one_plus_z*d1_co-d_co)
                hzlcdm = np.sqrt(Omega_m*np.power(1.+z,3)+1.-Omega_m)
                if np.any(hz/hzlcdm > self.max_hz_hzlcdm):
                    # np.set_printoptions(precision=5)
                    # print "Om0 %g" % Omega_m
                    # print "z %s" % np.str(xdata)
                    # print "d %s"%np.str(ydata)
                    # print "hz %s"%np.str(hz)
                    # print "hzlcdm %s"%np.str(hzlcdm)
                    # exit()
                    valid = False

        return valid


    def setXrange(self,Np):
        #self.xlum = ct.x_logspace(self.knots[0],self.knots[-1],Np)
        self.xlum = np.linspace(self.knots[0],self.knots[-1],Np)

        # Reset the boundary to coincide exactly with those of the knots.
        # Precision rounding from x_logspace() may cause small differences
        # which are troublesome when interpolating
        self.xlum[0] = self.knots[0]
        self.xlum[-1] = self.knots[-1]
