import numpy as np

"""
Given arrays x[0..n-1] and y[0..n-1] containing a tabulated function, i.e., y_i = f ( x_i ), with x_0 < x_1 < . . . < x_(n-1) , and given initial and final values y1_0 and y1_f for the first derivative of the interpolating function at points 0 and n-1 , respectively, this routine returns an array y2[0..n-1] that contains the second derivatives of the interpolating function at the tabulated points x_i . If y1_0 and/or y1_f are equal to 1e30 or larger, the routine is signaled to set the corresponding boundary condition for a natural spline, with zero second derivative on that boundary.

The function spline is called only once to process an entire tabulated function in arrays x_i and y_i . Once this has been done, values of the interpolated function for any value of x are obtained by calls (as many as desired) to a separate routine splint.

(c) Adapted from Numerical Recipes (ISBN 0-521-43108-5)
"""
def spline (x,y,y1_0,y1_f):

    if np.all(np.diff(x) > 0)==False :
        print "ERROR in spline(): the imput x array must be sorted in strictly ascending order!"
        exit()
    if len(x) != len(y) :
        print "ERROR in spline(): the x and y arrays must have the same size!"
        exit()

    n = len(y)

    y2 = [0]*n
    u  = [0]*(n-1)

    # The lower boundary condition is set either to be "natural"
    # or else to have a specified first derivative.
    if (y1_0 > 0.99e30):
        y2[0] = u[0] = 0.0
    else:
        y2[0] = -0.5;
        u[0]  = (3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-y1_0)

    # This is the decomposition loop of the tridiagonal algorithm.
    # y2 and u are used for temporary storage of the decomposed factors.
    for i in range(1,n-1,1):
        sig   = (x[i]-x[i-1]) / (x[i+1]-x[i-1])
        p     = sig*y2[i-1]+2.0
        y2[i] = (sig-1.0)/p
        u[i]  = (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1])
        u[i]  = (6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p

    # The upper boundary condition is set either to be "natural"
    # or else to have a specified first derivative.
    if (y1_f>0.99e30):
        qf = uf = 0.0
    else:
        qf = 0.5
        uf = (3.0/(x[n-1]-x[n-2]))*(y1_f-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]))

    y2[n-1] = (uf-qf*u[n-2])/(qf*y2[n-2]+1.0)

    # This is the backsubstitution loop of the tridiagonal algorithm
    for k in range(n-2,-1,-1):
        y2[k] = y2[k]*y2[k+1]+u[k]

    return y2


"""
Given the arrays xa[0..n-1] and ya[0..n-1] , which tabulate a function (with the xa_i's in order), and given the array y2a[0..n-1] , which is the output from spline above, and given a value of x , this routine returns a cubic-spline interpolated value y.

(c) Adapted from Numerical Recipes (ISBN 0-521-43108-5)
"""
def splint(xa,ya,y2a,x):

    if np.all(np.diff(xa) > 0)==False :
        print "ERROR in splint(): the imput x array must be sorted in strictly ascending order!"
        exit()
    if len(xa) != len(ya) :
        print "ERROR in splint(): the x and y arrays must have the same size!"
        exit()

    n = len(ya)

    # We will find the right place in the table by means of bisection.
    # This is optimal if sequential calls to this routine are at random
    # values of x. If sequential calls are in order, and closely spaced,
    # one would do better to store previous values of klo and khi and
    # test if they remain appropriate on the next call.
    klo = 0
    khi = n-1
    while (khi-klo>1):
        k=(khi+klo) >> 1
        if (xa[k] > x):
            khi = k
        else:
            klo = k

    # klo and khi now bracket the input value of x.
    h = xa[khi] - xa[klo]

    if (h==0.):
        print "ERROR in splint(): Bad xa input to routine splint. The xa's must be distinct."

    a = (xa[khi]-x)/h
    b = (x-xa[klo])/h

    # Cubic spline polynomial is now evaluated.
    y = a*ya[klo] + b*ya[khi] + ((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
    return y


# #TEST

# x=np.array([1.,2.,3.,4.,5.])
# y=np.exp(x)

# yin=np.exp(x[0])
# yfi=np.exp(x[-1])
# xp=4.5

# y2 = spline(x,y,yin,yfi)
# print("Exact: y=%g"%np.exp(xp))
# print("Spline: y=%g"%splint(x,y,y2,xp))
