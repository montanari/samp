"""
.. module:: JLA_lcdm
    :synopsis: JLA likelihood from Betoule et al. 2014

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

This is an adaptation of the original module written by Benjamin
Audren. In particular, instead of calling CLASS, a different module is
called to compute the model distance. This slightly speeds up further
the code.

Note that, since CLASS is not called, cosmological parameters should
be passed formally as nuisance parameters in the input parameter file.

Convention to compare with the original C++ code:

.. code::

    C00 = mag_covmat_file
    C11 = stretch_covmat_file
    C22 = colour_covmat_file
    C01 = mag_stretch_covmat_file
    C02 = mag_colour_covmat_file
    C12 = stretch_colour_covmat_file
"""


import numpy as np
import scipy.linalg as la

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
from montepython.likelihood_class import LikelihoodJLA


class JLA_lcdm(Likelihood):

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        self.JLA = LikelihoodJLA(self.data_directory, self.settings)

        # Load matrices from text files, whose names were read in the
        # configuration file
        self.C00 = self.JLA.read_matrix(self.JLA.mag_covmat_file)
        self.C11 = self.JLA.read_matrix(self.JLA.stretch_covmat_file)
        self.C22 = self.JLA.read_matrix(self.JLA.colour_covmat_file)
        self.C01 = self.JLA.read_matrix(self.JLA.mag_stretch_covmat_file)
        self.C02 = self.JLA.read_matrix(self.JLA.mag_colour_covmat_file)
        self.C12 = self.JLA.read_matrix(self.JLA.stretch_colour_covmat_file)

        # Reading light-curve parameters from self.data_file (jla_lcparams.txt)
        self.light_curve_params = self.JLA.read_light_curve_parameters()

        self.redshifts = self.light_curve_params.zcmb

        self.scriptmcut = self.JLA.scriptmcut

        if self.use_mock:
            self.mb_mock = self._get_mock_mb(self.light_curve_params,
                                             self.redshifts.size)


    def loglkl(self, cosmo, data):
        """
        Compute negative log-likelihood (eq.15 Betoule et al. 2014)

        """
        # Recover the distance moduli from CLASS (a size N vector of double
        # containing the predicted distance modulus for each SN in the JLA
        # sample, given the redshift of the supernova.)
        size = self.redshifts.size

        moduli = self._get_lcdm_moduli(data)

        # Convenience variables: store the nuisance parameters in short named
        # variables
        alpha = (data.mcmc_parameters['alpha']['current'] *
                 data.mcmc_parameters['alpha']['scale'])
        beta = (data.mcmc_parameters['beta']['current'] *
                data.mcmc_parameters['beta']['scale'])
        M = (data.mcmc_parameters['M']['current'] *
             data.mcmc_parameters['M']['scale'])
        Delta_M = (data.mcmc_parameters['Delta_M']['current'] *
                   data.mcmc_parameters['Delta_M']['scale'])

        C00, C11, C22 = self.C00, self.C11, self.C22
        C01, C02, C12 = self.C01, self.C02, self.C12
        covs = (C00, C11, C22, C01, C02, C12)
        albe = (alpha, beta)
        cov = self.JLA.get_cov(covs, albe)

        # This operation loops over all supernovae!
        # Compute the approximate moduli
        if self.use_mock:
            mb = self.mb_mock
        else:
            mb = self.light_curve_params.mb
        nuisances = (alpha, beta, M, Delta_M)
        data_moduli = self.JLA.get_moduli(mb,nuisances)

        # Compute the residuals (estimate of distance moduli - exact moduli)
        residuals = data_moduli - moduli

        # Whiten the residuals, in two steps:
        #
        # 1) Compute the Cholesky decomposition of the covariance
        # matrix, in place. This is a time expensive (0.015 seconds)
        # part.
        cov = la.cholesky(cov, lower=True, overwrite_a=True)

        # 2) Solve the triangular system, also time expensive.
        residuals = la.solve_triangular(cov, residuals, lower=True,
                                        check_finite=False)

        # Compute the chi2 as the sum of the squared residuals.
        chi2 = (residuals**2).sum()

        return -0.5 * chi2


    def _get_angular_distance(self, data):
        """Reduced angular diameter distance d_A = H0*D_A, for LCDM."""

        # The result will not depend on this parameter. Need to set it
        # just to call correctly the cosmological module.
        h = 0.7
        H0_iMpc = h / 2997.9

        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])
        for nuisance in nuisance_parameter_names:
            if nuisance == 'Omega_m':
                Omega_m = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'Omega_k':
                Omega_k = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']

        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h' : h,
                 'Omega_m' : Omega_m,
                 'Omega_k' : Omega_k}
        LambdaCDM.set_cosmo(cosmo)

        # d_A = H0*D_A, does not depend on the choice made for h.
        dA = LambdaCDM.angular_distance(self.redshifts) * H0_iMpc

        return dA


    def _get_lcdm_moduli(self, data):
        """LCDM distance modulus."""
        luminosity_distance = ((1. + self.redshifts)**2
                               * self._get_angular_distance(data))
        moduli = 5. * np.log10(luminosity_distance) + self.M0

        return moduli


    def _get_mock_mb(self, light_curve_params, zsize):
        """Return LCDM apparent B-band magnitude."""

        # Set fiducial parameters
        LambdaCDM = cc.LambdaCDM()
        mock = {'Omega_m': 0.3,
                 'Omega_Lambda': 0.7,
                 'h': 0.7}
        LambdaCDM.set_cosmo(mock)
        M = -19.05
        alpha = 0.141
        beta = 3.101
        Delta_M = -0.07

        # Retrieve the cosmology.
        moduli = np.empty((zsize, ))
        for index, row in light_curve_params.iterrows():
            z_cmb = row['zcmb']
            luminosity_distance = ((1 + z_cmb)**2 *
                                   LambdaCDM.angular_distance([z_cmb])) # Mpc
            moduli[index] = luminosity_distance * 1.e6 # Pc
        moduli = 5. * np.log10(moduli/10.)

        mb = (moduli + M - alpha*light_curve_params.x1
              + beta*light_curve_params.color
              + Delta_M*(light_curve_params.thirdvar > self.scriptmcut))

        return mb
