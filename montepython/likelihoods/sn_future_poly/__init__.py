""".. module:: sn_future_poly
    :synopsis: Polynomial likelihood for futuristic SNIa experiment

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Mock data are distributed over a logarithmic interval in redshift.

"""

import os

import numpy as np
from scipy.signal import argrelmax, argrelmin

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
import montepython.polytools as pt

class sn_future_poly(Likelihood):
    def __init__(self, path, data, command_line):
        Likelihood.__init__(self, path, data, command_line)
        self.z, self.mu, self.mu_err = self._get_mock()


    def loglkl(self, cosmo, data):

        dL = self._get_poly_dL(data)

        difference = 5.*np.log10(dL) - self.mu
        chi_squared = np.sum(difference**2. / self.mu_err**2.)

        # return ln(L)
        lkl = - 0.5 * chi_squared

        return lkl


    def _get_mock(self):
        """Return z, dL, dL_err for a LCDM mock cosmology."""

        hubble = self.H0/100.

        # Set fiducial parameters.
        LambdaCDM = cc.LambdaCDM()
        mock = {'Omega_m': self.Omega_m,
                 'Omega_Lambda': self.Omega_Lambda,
                 'h': hubble}
        LambdaCDM.set_cosmo(mock)

        H0_iMpc = hubble * LambdaCDM.h_to_H0 # 1/Mpc

        # Log-spaced redshifts.
        reds = np.logspace(np.log10(self.zmin), np.log10(self.zmax),
                           self.nz)

        # Distance modulus.
        DL = ((1. + reds)**2 * LambdaCDM.angular_distance(reds)) # Mpc
        dL = DL * H0_iMpc

        mu = 5. * np.log10(dL)

        # The fractional error is set according the the mean
        # fractional error of JLA apparent magnitude (0.0045 \pm
        # 0.001).
        mu_err = np.abs(0.0045 * mu)

        return reds, mu, mu_err


    def _get_poly_dL(self, data):
        # Technically all parameters varying in the likelihood, but not in the
        # Boltzmann code, are all nuisance parameters
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])
        derived_nuisance_names = data.get_mcmc_parameters(['derived_nuisance'])

        # fill with nuisance parameters
        c4 = 0
        c5 = 0
        for nuisance in nuisance_parameter_names:
            if nuisance == 'c2':
                c2 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c3':
                c3 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c4':
                c4 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c5':
                c5 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']


        coeff = (c2, c3, c4, c5)
        dL = pt.dL_poly(self.z, coeff, self.fit_distance)[0]

        if self.validate_dL(c2,c3,c4,c5) is False:
            dL = [1.e30]*len(dL)
            dL = np.asarray(dL)
        elif self.sachs_condition is True:
            val = self.validate_sachs(c2,c3,c4,c5)
            if val is False:
                dL = [1.e30]*len(dL)
                dL = np.asarray(dL)

        return dL


    def set_validate_range(self, Np=500):
        """Return an ordered array of redshift points.

        Notice
        ------
        d'_A=0 at a z outside the Union/JLA data range.
        Set here a max redshift large enough to test the point d'_A=0.
        This is an extrapolation and should be used ONLY to implement physical
        conditions on the possible polynomial distance sets.
        The distance itself outside the data range must not be used, e.g.,
        to constrain cosmological parameters.

        """
        sortId = np.argsort(self.z)
        zsort  = self.z[sortId]

        zmin, zmax = self.validation_range

        z = np.linspace(zmin,zmax,Np)

        return z

    def validate_dL(self, c2, c3, c4, c5):
        """Add penalization to the likelihood if angular diameter
        distance is negative and if has more than one maximum.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        valid : bool
            True if the Sachs equation can be computed, False otherwise.
        """

        validate = True

        z = self.set_validate_range()

        coeff = (c2,c3,c4, c5)
        dL = pt.dL_poly(z, coeff, self.fit_distance)[0]

        # dL must be positive
        if np.any(dL < 0):
            validate = False

        # dA can have at most one extreme (maximum)
        #
        # sort the data in x and use that to rearrange y
        if validate is True:
            sortId = np.argsort(z)
            zsort  = z[sortId]
            dLsort = dL[sortId]
            dA = dLsort / np.power(1.+zsort,2)
            imax = argrelmax(dA)[0]
            imin = argrelmin(dA)[0]
            Nextrm = len(imax) + len(imin)
            if Nextrm > 1:
                validate = False

        # No sign change in dA''.
        # This serves mostly as test, since these curves are anyway
        # excluded when the following is required: dA'=0 once and only
        # once, h^2>0.
        check_dd_dA = False
        if validate and check_dd_dA:
            from scipy import interpolate
            from scipy.misc import derivative
            dd_dA = np.asarray([])
            f = interpolate.interp1d(z,dA)
            for i, zi in enumerate(z):
                if i>0 and i<len(z)-1:
                    dd_dA = np.append(dd_dA,
                                      derivative(f,zi,dx=1e-6,n=2))
            pos = np.any(dd_dA>0)
            neg = np.any(dd_dA<0)
            if pos and neg:
                validate = False

        return validate


    def validate_sachs(self, c2, c3, c4, c5):
        """ The Sachs equation involve the square root of the quantity here
        computed. If the latter a negative, return a False validation.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        valid : bool
                True if the Sachs equation can be computed, False otherwise.
        """
        valid = True

        z = self.set_validate_range()

        coeff = (c2,c3,c4,c5)

        dL, d1L, d2L = pt.dL_poly(z, coeff, self.fit_distance,
                                         deriv=True)

        # Comoving distance
        d_co  = dL/(1.+z)
        d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

        z0 = pt.sachs_z0(coeff, (z[0],z[-1]), self.fit_distance)
        if z0:
            Omega_m = pt.sachs_Omega_m(z0, coeff, self.fit_distance)
            h2_num = pt.sachs_h2_num(z, coeff, Omega_m,
                                            self.fit_distance)

            # If the angular diameter distance dA has at most one
            # maximum, and if Omega_m can be computed (which in this
            # case corresponds to requiring that dA has one maximum),
            # then the square of the Hubble parameter numerator must
            # necessarily be positive. Unless dA changes curvature at
            # a z (without maxima). In this case h2_num can still be
            # negative.
            if np.any(h2_num < 0):
                valid = False
        else:
            valid = False

        return valid


    def Omega_m_sachs_rand(self, c2, c3, c4, c5):
        """ Retrieve Omega_m by requiring a finite Hubble
        parameter. If the angular diameter distance has no maximum,
        return a random value Omega_m>=0, bounded from the top by
        requiring a positive square numerator of the Hubble parameter
        and Omega_m<1.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        Omega_m : float or None
            Return Omega_m value, or None.
        """
        import random

        maxiter = 100
        z = self.set_validate_range()

        coeff = (c2,c3,c4,c5)

        dL, d1L, d2L = pt.dL_poly(z, coeff, self.fit_distance,
                                         deriv=True)

        # Comoving distance
        d_co  = dL/(1.+z)
        d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

        z0 = pt.sachs_z0(coeff, (z[0],z[-1]), self.fit_distance)

        if z0:
            Omega_m = pt.sachs_Omega_m(z0, coeff, self.fit_distance)
            h2_num = pt.sachs_h2_num(z, coeff, Omega_m,
                                            self.fit_distance)

            if np.any(h2_num < 0):
                Omega_m = None
        else:
            rand = np.random.rand(maxiter)
            for Omm in rand:
                h2_num = pt.sachs_h2_num(z, coeff, Omm,
                                                self.fit_distance)
                if np.all(h2_num > 0):
                    Omega_m = Omm
                    break
                else:
                    Omega_m = None

        return Omega_m
