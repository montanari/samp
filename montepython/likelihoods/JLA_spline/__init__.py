"""
.. module:: JLA
    :synopsis: Spline JLA likelihood from Betoule et al. 2014

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Based on Benjamin Audren's adaptation of the original C++ code available at
`this address <http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html>`_.
Here is the correspondence between original file names and variables:

.. code::

    C00 = mag_covmat_file
    C11 = stretch_covmat_file
    C22 = colour_covmat_file
    C01 = mag_stretch_covmat_file
    C02 = mag_colour_covmat_file
    C12 = stretch_colour_covmat_file

.. note::

    The `pandas` and `numexpr` python packages must be
    installed. These are used to considerably improve execution time
    over Python native or Numpy implementations. In particular,
    `numexpr` replaces the blas daxpy function for arrays manipulation
    in the original C++ code.
"""

import os
import re

import numpy as np
from scipy import integrate
from scipy import interpolate
import scipy.linalg as la
from scipy.misc import derivative
from scipy.signal import argrelmax, argrelmin

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
from montepython.likelihood_class import LikelihoodJLA
import montepython.manipulate_strings as mstr
import montepython.splinetools as st


class JLA_spline(Likelihood):

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        self.JLA = LikelihoodJLA(self.data_directory, self.settings)

        # Load matrices from text files, whose names were read in the
        # configuration file.
        self.C00 = self.JLA.read_matrix(self.JLA.mag_covmat_file)
        self.C11 = self.JLA.read_matrix(self.JLA.stretch_covmat_file)
        self.C22 = self.JLA.read_matrix(self.JLA.colour_covmat_file)
        self.C01 = self.JLA.read_matrix(self.JLA.mag_stretch_covmat_file)
        self.C02 = self.JLA.read_matrix(self.JLA.mag_colour_covmat_file)
        self.C12 = self.JLA.read_matrix(self.JLA.stretch_colour_covmat_file)

        self.scriptmcut = self.JLA.scriptmcut

        # Reading light-curve parameters from self.data_file
        # (jla_lcparams.txt).
        self.light_curve_params = self.JLA.read_light_curve_parameters()

        self.redshifts = self.light_curve_params.zcmb
        self.validation_z = self._set_validation_range()

        if self.use_mock:
            self.mb_mock = self._get_mock_mb(self.light_curve_params,
                                             self.redshifts.size)

        # Compute spline knots and save them on log.param.
        if self.dictionary['knots'] == 'auto':
            log_path = os.path.join(command_line.folder, 'log.param')
            self._set_knots(log_path)

        self.y_0 = 0. # Initial condition dL(z=0)=0
        self.y1_0 = 1. # Initial condition dL'(z=0)=1


    def loglkl(self, cosmo, data):
        """Compute negative log-likelihood (eq.15 Betoule et al. 2014).
        """
        size = self.redshifts.size

        moduli = self._get_spline_moduli(data)

        alpha = (data.mcmc_parameters['alpha']['current'] *
                 data.mcmc_parameters['alpha']['scale'])
        beta = (data.mcmc_parameters['beta']['current'] *
                data.mcmc_parameters['beta']['scale'])
        M = (data.mcmc_parameters['M']['current'] *
             data.mcmc_parameters['M']['scale'])
        Delta_M = (data.mcmc_parameters['Delta_M']['current'] *
                   data.mcmc_parameters['Delta_M']['scale'])

        C00, C11, C22 = self.C00, self.C11, self.C22
        C01, C02, C12 = self.C01, self.C02, self.C12
        covs = (C00, C11, C22, C01, C02, C12)
        albe = (alpha, beta)
        cov = self.JLA.get_cov(covs, albe)

        # This operation loops over all supernovae!
        # Compute the approximate moduli
        if self.use_mock:
            mb = self.mb_mock
        else:
            mb = self.light_curve_params.mb
        nuisances = (alpha, beta, M, Delta_M)
        data_moduli = self.JLA.get_moduli(mb, nuisances)

        # Compute the residuals (estimate of distance moduli - exact moduli)
        residuals = data_moduli - moduli

        # Whiten the residuals, in two steps:
        #
        # 1) Compute the Cholesky decomposition of the covariance
        # matrix, in place. This is a time expensive part.
        cov = la.cholesky(cov, lower=True, overwrite_a=True)

        # 2) Solve the triangular system, also time expensive.
        residuals = la.solve_triangular(cov, residuals, lower=True,
                                        check_finite=False)

        # Compute the chi2 as the sum of the squared residuals.
        chi2 = (residuals**2).sum()

        # Add roughness parameter.
        chi2 += self._get_rough()

        return -0.5 * chi2


    def _set_knots(self, log_path):
        """Set the spline knots on a logarithmic scale.

        Parameters
        ----------
        log_path : string
                   path of the log.param file
        """
        # Sort z array in ascending order and read initial/final
        # redshifts.
        z_sorted = sorted(self.redshifts)
        logz_start = np.log10(z_sorted[0])
        logz_stop = np.log10(z_sorted[-1])

        # Define increasing spline knots on log scale.
        self.knots = np.logspace(logz_start, logz_stop,
                                 num=len(self.spline_params),
                                 base=10.0)

        # Write the knots to the .data file to record correctly
        # log.param.
        karrstr = np.char.mod('%f', self.knots) # String array
        kstr = "["+", ".join(karrstr)+"]\n" # Combine to a string
        klabel = self.name+'.knots'
        mstr.replace(log_path, '(?m)^'+re.escape(klabel)+'.*\n?',
                     klabel+'='+kstr)

        # Update also the dictionary.
        self.dictionary['knots'] = self.knots


    def _get_spline_moduli(self, data):
        """Retrieve moduli at spline knots.
        """
        # Read spline parameters.
        coeff = []
        for dname in self.spline_params:
            setname = ("{name} = (data.mcmc_parameters['{name}']['current']"
                       "* data.mcmc_parameters['{name}']['scale'])")
            exec(setname.format(name=dname))
            exec("coeff += [%s]" % dname)

        # Spline slope at knot.
        # self.y1_0 = (data.mcmc_parameters['y1_0']['current'] *
        #              data.mcmc_parameters['y1_0']['scale'])
        self.y1_f = (data.mcmc_parameters['y1_f']['current'] *
                     data.mcmc_parameters['y1_f']['scale'])

        # Consistency check: how many parameters starting by 'd'?
        paramnames = data.get_mcmc_parameters(['nuisance'])
        nparam = 0
        for i, p in enumerate(paramnames):
            if p[0]=='d':
                nparam += 1
        if (nparam != len(coeff)):
            msg = ("The number of spline parameters does not match the "
                   "number of parameters starting by 'd'. If this is "
                   "expected, please remove this error message.")
            raise RuntimeError(msg)

        self.spl_coeff = np.asarray(coeff)

        # Spline points to be fitted
        dL, __ = st.spline_p0(self.redshifts, self.knots, self.spl_coeff,
                              self.y_0, deriv_0=(self.y1_0, 1),
                              deriv_f=(self.y1_f, 1))

        moduli = 5.0 * np.log10(dL) + self.M0

        valid_dL = self.validate_dL(dL)

        if valid_dL is False:
            moduli = np.asarray([1.e30]*len(moduli))

        return moduli


    def _set_validation_range(self, Np=500):
        """Return an ordered array of redshift points."""
        zsort = sorted(self.redshifts)
        validation_z = np.linspace(zsort[0], zsort[-1], Np)

        return validation_z


    def validate_dL(self, dL):
        """Add penalization to the likelihood if angular diameter
        distance is negative and if it has more than one maximum.

        Return
        ------
        valid : bool
            True if the Sachs equation can be computed, False otherwise.
        """

        validate = True

        # Spline points to be fitted
        dL, self.spl_diff2 = st.spline_p0(self.validation_z, self.knots,
                                          self.spl_coeff, self.y_0,
                                          deriv_0=(self.y1_0, 1),
                                          deriv_f=(self.y1_f, 1))

        # dL must be positive
        if np.any(dL < 0):
            validate = False

        # dA can have at most one extreme (maximum).
        #
        # Sort the data in x and use that to rearrange y.
        if validate is True:
            sortId = np.argsort(self.validation_z)
            zsort  = self.validation_z[sortId]
            dLsort = dL[sortId]
            dA = dLsort / np.power(1.+zsort, 2.)
            imax = argrelmax(dA)[0]
            imin = argrelmin(dA)[0]
            Nextrm = len(imax) + len(imin)
            if Nextrm > 1:
                validate = False

        # There must be no sign change in dA''.
        check_dd_dA = False
        if validate and check_dd_dA:
            dd_dA = np.asarray([])
            f = interpolate.interp1d(z,dA)
            for i, zi in enumerate(z):
                if i>0 and i<len(z)-1:
                    dd_dA = np.append(dd_dA,
                                      derivative(f,zi,dx=1e-6,n=2))
            pos = np.any(dd_dA>0)
            neg = np.any(dd_dA<0)
            if pos and neg:
                validate = False

        return validate


    def _get_mock_mb(self, light_curve_params, zsize):
        """Return LCDM apparent B-band magnitude."""

        # Set fiducial parameters
        LambdaCDM = cc.LambdaCDM()
        mock = {'Omega_m': 0.3,
                 'Omega_Lambda': 0.7,
                 'h': 0.7}
        LambdaCDM.set_cosmo(mock)
        M = -19.05
        alpha = 0.141
        beta = 3.101
        Delta_M = -0.07

        # Retrieve the cosmology.
        moduli = np.empty((zsize, ))
        for index, row in light_curve_params.iterrows():
            z_cmb = row['zcmb']
            luminosity_distance = ((1. + z_cmb)**2
                                   * LambdaCDM.angular_distance([z_cmb])) # Mpc
            moduli[index] = luminosity_distance * 1.e6 # Pc
        moduli = 5. * np.log10(moduli/10.)

        mb = (moduli + M - alpha*light_curve_params.x1
              + beta*light_curve_params.color
              + Delta_M*(light_curve_params.thirdvar > self.scriptmcut))

        return mb


    def _rough_integrand(self, z):
        """Integrand to compute the roughness factor."""
        # int (d''(ln z))**2 d ln z, where d ln z = dz/z
        return self.dL_prime_prime(z)**2. / z


    def _get_rough(self):
        """Add roughness parameter."""
        #Remember that cubic splines have linear second derivatives.
        if self.rough_lambda < 0.:
            raise RuntimeError("Roughness parameter must be non-negative")
        elif self.rough_lambda == 0:
            rough = 0.
        else:
            # Second derivatives of cubic splines are linear.
            self.dL_prime_prime = interpolate.interp1d(self.knots,
                                                       self.spl_diff2,
                                                       kind='slinear')
            result, __ = integrate.quad(self._rough_integrand,
                                        self.knots[0], self.knots[-1])
            rough = self.rough_lambda * result

        return rough
