""".. module:: hubble_const_kH
    :synopsis: Hubble parameter fit given constant curvature

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Fit Hubble parameter h(z)=H(z)/H0 data, given a constant curvature
parameter kH and a polynomial distance fit.

"""

import os
import numpy as np

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
import montepython.polytools as pt

class hubble_const_kH(Likelihood):
    """Fit Hubble parameter h(z)=H(z)/H0 data, given a constant curvature
    parameter kH and a polynomial distance fit.

    """

    def __init__(self, path, data, command_line):
        Likelihood.__init__(self, path, data, command_line)

        # Read the content of the data file, containing z, Hz and error
        total = np.loadtxt(os.path.join(self.data_directory,
                                        self.data_file))

        # Store the columns separately
        self.z = total[:, 0]
        self.Hz_err = total[:, 2]
        if self.use_mock:
            self.Hz = self._get_mock_Hubble()
        else:
            self.Hz = total[:, 1]

        # Check distance physical conditions.
        self.validation_redshifts = self._set_validation_range()


    def loglkl(self, cosmo, data):
        """Main method."""
        coeff = self._get_coeff(data)
        H0 = self._get_H0(data)
        kH = self._get_kH(data)

        dL, d1L, __ = pt.dL_poly(self.z, coeff, self.fit_distance,
                                 deriv=True)
        valid_dL = pt.validate_dL(self.validation_redshifts, coeff,
                                  self.fit_distance)

        hz = pt.hubble_const_kH(self.z, dL, d1L, kH)

        if (hz is not None) and valid_dL:
            chi2 = np.sum((self.Hz - H0*hz)**2. / self.Hz_err**2.)
        else:
            chi2 = 1.e30

        lkl = - 0.5 * chi2

        return lkl

    def _get_coeff(self, data):
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])

        # fill with nuisance parameters
        c4 = 0
        c5 = 0
        for nuisance in nuisance_parameter_names:
            if nuisance == 'c2':
                c2 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c3':
                c3 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c4':
                c4 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c5':
                c5 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']

        coeff = (c2, c3, c4, c5)

        return coeff

    def _get_H0(self, data):
        """Hubble constant."""
        H0 = (data.mcmc_parameters['H0']['current']
              * data.mcmc_parameters['H0']['scale'])
        return H0

    def _get_kH(self, data):
        """Curvature parameter."""
        kH = (data.mcmc_parameters['kH']['current']
              * data.mcmc_parameters['kH']['scale'])
        return kH

    def _get_mock_Hubble(self):
        """Return LCDM Hubble parameter in km/s/Mpc."""

        LambdaCDM = cc.LambdaCDM()
        cosmo = {'Omega_m': 0.7,
                 'Omega_Lambda': 0.7,
                 'h': 0.7}
        LambdaCDM.set_cosmo(cosmo)

        Hubble = LambdaCDM.H(self.z)

        return Hubble

    def _set_validation_range(self, Np=500):
        """Return an ordered array of redshift points."""
        validation_z = np.linspace(min(self.z), max(self.z), Np)
        return validation_z
