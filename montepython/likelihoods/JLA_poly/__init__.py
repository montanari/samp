"""
.. module:: JLA_poly
    :synopsis: Polynomial JLA likelihood from Betoule et al. 2014

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Based on Benjamin Audren's adaptation of the original C++ code available at
`this address <http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html>`_.
Here is the correspondence between original file names and variables:

.. code::

    C00 = mag_covmat_file
    C11 = stretch_covmat_file
    C22 = colour_covmat_file
    C01 = mag_stretch_covmat_file
    C02 = mag_colour_covmat_file
    C12 = stretch_colour_covmat_file

.. note::

    The `pandas` and `numexpr` python packages must be
    installed. These are used to considerably improve execution time
    over Python native or Numpy implementations. In particular,
    `numexpr` replaces the blas daxpy function for arrays manipulation
    in the original C++ code.
"""
import numpy as np
import scipy.linalg as la
import montepython.polytools as pt
from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
from montepython.likelihood_class import LikelihoodJLA


class JLA_poly(Likelihood):

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        self.JLA = LikelihoodJLA(self.data_directory, self.settings)

        # Load matrices from text files, whose names were read in the
        # configuration file
        self.C00 = self.JLA.read_matrix(self.JLA.mag_covmat_file)
        self.C11 = self.JLA.read_matrix(self.JLA.stretch_covmat_file)
        self.C22 = self.JLA.read_matrix(self.JLA.colour_covmat_file)
        self.C01 = self.JLA.read_matrix(self.JLA.mag_stretch_covmat_file)
        self.C02 = self.JLA.read_matrix(self.JLA.mag_colour_covmat_file)
        self.C12 = self.JLA.read_matrix(self.JLA.stretch_colour_covmat_file)

        self.scriptmcut = self.JLA.scriptmcut

        # Reading light-curve parameters from self.data_file (jla_lcparams.txt)
        self.light_curve_params = self.JLA.read_light_curve_parameters()

        self.redshifts = self.light_curve_params.zcmb
        self.validation_redshifts = self._set_validation_range()

        if self.use_mock:
            self.mb_mock = self._get_mock_mb(self.light_curve_params,
                                        self.redshifts.size)

    def loglkl(self, cosmo, data):
        """
        Compute negative log-likelihood (eq.15 Betoule et al. 2014)

        """
        size = self.redshifts.size

        moduli = self._get_poly_moduli(data)

        alpha = (data.mcmc_parameters['alpha']['current'] *
                 data.mcmc_parameters['alpha']['scale'])
        beta = (data.mcmc_parameters['beta']['current'] *
                data.mcmc_parameters['beta']['scale'])
        M = (data.mcmc_parameters['M']['current'] *
             data.mcmc_parameters['M']['scale'])
        Delta_M = (data.mcmc_parameters['Delta_M']['current'] *
                   data.mcmc_parameters['Delta_M']['scale'])

        C00, C11, C22 = self.C00, self.C11, self.C22
        C01, C02, C12 = self.C01, self.C02, self.C12
        covs = (C00, C11, C22, C01, C02, C12)
        albe = (alpha, beta)
        cov = self.JLA.get_cov(covs, albe)

        # This operation loops over all supernovae!
        # Compute the approximate moduli
        if self.use_mock:
            mb = self.mb_mock
        else:
            mb = self.light_curve_params.mb
        nuisances = (alpha, beta, M, Delta_M)
        data_moduli = self.JLA.get_moduli(mb, nuisances)

        # Compute the residuals (estimate of distance moduli - exact moduli)
        residuals = data_moduli - moduli

        # Whiten the residuals, in two steps:
        #
        # 1) Compute the Cholesky decomposition of the covariance
        # matrix, in place. This is a time expensive part.
        cov = la.cholesky(cov, lower=True, overwrite_a=True)

        # 2) Solve the triangular system, also time expensive.
        residuals = la.solve_triangular(cov, residuals, lower=True,
                                        check_finite=False)

        # Compute the chi2 as the sum of the squared residuals.
        chi2 = (residuals**2).sum()

        if self.Lya is True:
            dA_Lya_poly = (pt.dL_poly(self.z_Lya, self.coeff,
                                             self.fit_distance)[0]
                           / (1+self.z_Lya)**2)
            chi2 += ((dA_Lya_poly-self.dA_Lya) / self.err_dA_Lya)**2


        return -0.5 * chi2


    def _get_poly_moduli(self, data):
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])

        # fill with nuisance parameters
        c4 = 0
        c5 = 0
        for nuisance in nuisance_parameter_names:
            if nuisance == 'c2':
                c2 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c3':
                c3 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c4':
                c4 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c5':
                c5 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']


        self.coeff = (c2,c3,c4,c5)
        dL = pt.dL_poly(self.redshifts, self.coeff, self.fit_distance)[0]

        moduli = 5.0 * np.log10(dL) + self.M0

        z_val = self.validation_redshifts

        valid_dL = pt.validate_dL(z_val, self.coeff, self.fit_distance)

        if valid_dL is False:
            moduli = np.asarray([1.e30]*len(moduli))
        elif self.sachs_condition is True:
            valid_sachs = pt.validate_sachs(z_val,
                                            self.coeff,
                                            self.fit_distance)
            if valid_sachs is False:
                moduli = np.asarray([1.e30]*len(moduli))

        return moduli


    def _set_validation_range(self,Np=500):
        """Return an ordered array of redshift points."""

        # d'_A=0 at a z outside the Union/JLA data range.  Set here a
        # max redshift large enough to test the point d'_A=0.  This is
        # an extrapolation and should be used ONLY to implement
        # physical conditions on the possible polynomial distance
        # sets.  The distance itself outside the data range must not
        # be used to constrain cosmological parameters.
        zmin, zmax = self.validation_range

        validation_z = np.linspace(zmin,zmax,Np)

        return validation_z


    def _get_mock_mb(self, light_curve_params, zsize):
        """Return LCDM apparent B-band magnitude."""

        # Set fiducial parameters
        LambdaCDM = cc.LambdaCDM()
        mock = {'Omega_m': 0.3,
                 'Omega_Lambda': 0.7,
                 'h': 0.7}
        LambdaCDM.set_cosmo(mock)
        M = -19.05
        alpha = 0.141
        beta = 3.101
        Delta_M = -0.07

        # Retrieve the cosmology.
        moduli = np.empty((zsize, ))
        for index, row in light_curve_params.iterrows():
            z_cmb = row['zcmb']
            luminosity_distance = ((1. + z_cmb)**2
                                   * LambdaCDM.angular_distance([z_cmb])) # Mpc
            moduli[index] = luminosity_distance * 1.e6 # Pc
        moduli = 5. * np.log10(moduli/10.)

        mb = (moduli + M - alpha*light_curve_params.x1
              + beta*light_curve_params.color
              + Delta_M*(light_curve_params.thirdvar > self.scriptmcut))

        return mb
