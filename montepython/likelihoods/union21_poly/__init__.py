import os
import numpy as np
from scipy.signal import argrelmax, argrelmin
from montepython.likelihood_class import Likelihood
import montepython.polytools as pt

class union21_poly(Likelihood):

    # initialization routine

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        # define array for values of z and data points
        self.z = np.array([], 'float64')
        self.moduli = np.array([], 'float64')

        # read redshifts and data points
        for line in open(os.path.join(
                self.data_directory, self.z_mu_dmu), 'r'):
            if (line.find('#') == -1):
                self.z = np.append(self.z, float(line.split()[1]))
                self.moduli = np.append(self.moduli, float(line.split()[2]))

        # number of data points
        self.num_points = np.shape(self.z)[0]

        # define correlation matrix
        covmat = np.zeros((self.num_points, self.num_points), 'float64')

        # file containing correlation matrix
        covmat_filename = self.covmat_sys

        # read correlation matrix
        i = 0
        for line in open(os.path.join(
                self.data_directory, covmat_filename), 'r'):
            if (line.find('#') == -1):
                covmat[i] = line.split()
                i += 1

        # invert correlation matrix
        self.inv_covmat = np.linalg.inv(covmat)


    def loglkl(self, cosmo, data):

        dL = self._get_poly_dL(data)

        # define array for difference between theory and observations
        difference = np.ndarray(self.num_points, 'float64')
        for i in range(self.num_points):
            difference[i] = 5 * np.log10(dL[i]) + self.M - self.moduli[i]

        chi_squared = np.dot(difference, np.dot(self.inv_covmat, difference))
        # chi_squared = np.dot(difference,difference)

        if self.Lya is True:
            dA_Lya_poly = (pt.dL_poly(self.z_Lya, coeff,
                                             self.fit_distance)[0]
                           / (1+self.z_Lya)**2)
            chi_squared += ((dA_Lya_poly-self.dA_Lya) / self.err_dA_Lya)**2

        # return ln(L)
        lkl = - 0.5 * chi_squared

        return lkl


    def _get_poly_dL(self, data):
        # Technically all parameters varying in the likelihood, but not in the
        # Boltzmann code, are all nuisance parameters
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])
        derived_nuisance_names = data.get_mcmc_parameters(['derived_nuisance'])

        # fill with nuisance parameters
        c4 = 0
        c5 = 0
        for nuisance in nuisance_parameter_names:
            if nuisance == 'c2':
                c2 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c3':
                c3 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c4':
                c4 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']
            elif nuisance == 'c5':
                c5 = data.mcmc_parameters[nuisance]['current']\
                     *data.mcmc_parameters[nuisance]['scale']


        coeff = (c2,c3,c4,c5)
        dL = pt.dL_poly(self.z,coeff, self.fit_distance)[0]

        if self.validate_dL(c2,c3,c4,c5) is False:
            dL = [1.e30]*len(dL)
            dL = np.asarray(dL)
        elif self.sachs_condition is True:
            val = self.validate_sachs(c2,c3,c4,c5)
            if val is False:
                dL = [1.e30]*len(dL)
                dL = np.asarray(dL)

        # #The following is only useful for testing
        # Omega_m = self.Omega_m_sachs_rand(c2,c3,c4,c5)
        # if Omega_m:
        #     for param in derived_nuisance_names:
        #         if param == 'Omega_m':
        #             scale = data.mcmc_parameters[param]['scale']
        #             data.mcmc_parameters[param]['current'] = Omega_m / scale
        # else:
        #     dL = [1.e30]*len(dL)
        #     dL = np.asarray(dL)

        return dL


    def set_validate_range(self,Np=500):
        """Return an ordered array of redshift points.

        Notice
        ------
        d'_A=0 at a z outside the Union/JLA data range.
        Set here a max redshift large enough to test the point d'_A=0.
        This is an extrapolation and should be used ONLY to implement physical
        conditions on the possible polynomial distance sets.
        The distance itself outside the data range must not be used, e.g.,
        to constrain cosmological parameters.

        """
        sortId = np.argsort(self.z)
        zsort  = self.z[sortId]

        zmin, zmax = self.validation_range

        z = np.linspace(zmin,zmax,Np)

        return z

    def validate_dL(self,c2, c3, c4, c5):
        """Add penalization to the likelihood if angular diameter
        distance is negative and if has more than one maximum.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        valid : bool
            True if the Sachs equation can be computed, False otherwise.
        """

        validate = True

        z = self.set_validate_range()

        coeff = (c2,c3,c4, c5)
        dL = pt.dL_poly(z, coeff, self.fit_distance)[0]

        # dL must be positive
        if np.any(dL < 0):
            validate = False

        # dA can have at most one extreme (maximum)
        #
        # sort the data in x and use that to rearrange y
        if validate is True:
            sortId = np.argsort(z)
            zsort  = z[sortId]
            dLsort = dL[sortId]
            dA = dLsort / np.power(1.+zsort,2)
            imax = argrelmax(dA)[0]
            imin = argrelmin(dA)[0]
            Nextrm = len(imax) + len(imin)
            if Nextrm > 1:
                validate = False

        # No sign change in dA''.
        # This serves mostly as test, since these curves are anyway
        # excluded when the following is required: dA'=0 once and only
        # once, h^2>0.
        check_dd_dA = False
        if validate and check_dd_dA:
            from scipy import interpolate
            from scipy.misc import derivative
            dd_dA = np.asarray([])
            f = interpolate.interp1d(z,dA)
            for i, zi in enumerate(z):
                if i>0 and i<len(z)-1:
                    dd_dA = np.append(dd_dA,
                                      derivative(f,zi,dx=1e-6,n=2))
            pos = np.any(dd_dA>0)
            neg = np.any(dd_dA<0)
            if pos and neg:
                validate = False

        return validate


    def validate_sachs(self, c2, c3, c4, c5):
        """ The Sachs equation involve the square root of the quantity here
        computed. If the latter a negative, return a False validation.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        valid : bool
                True if the Sachs equation can be computed, False otherwise.
        """
        valid = True

        z = self.set_validate_range()

        coeff = (c2,c3,c4,c5)

        dL, d1L, d2L = pt.dL_poly(z, coeff, self.fit_distance,
                                         deriv=True)

        # Comoving distance
        d_co  = dL/(1.+z)
        d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

        z0 = pt.sachs_z0(coeff, (z[0],z[-1]), self.fit_distance)
        if z0:
            Omega_m = pt.sachs_Omega_m(z0, coeff, self.fit_distance)
            h2_num = pt.sachs_h2_num(z, coeff, Omega_m,
                                            self.fit_distance)

            # If the angular diameter distance dA has at most one
            # maximum, and if Omega_m can be computed (which in this
            # case corresponds to requiring that dA has one maximum),
            # then the square of the Hubble parameter numerator must
            # necessarily be positive. Unless dA changes curvature at
            # a z (without maxima). In this case h2_num can still be
            # negative.
            if np.any(h2_num < 0):
                valid = False
        else:
            valid = False

        return valid


    def Omega_m_sachs_rand(self, c2, c3, c4, c5):
        """ Retrieve Omega_m by requiring a finite Hubble
        parameter. If the angular diameter distance has no maximum,
        return a random value Omega_m>=0, bounded from the top by
        requiring a positive square numerator of the Hubble parameter
        and Omega_m<1.

        Parameters
        ----------
        c2, c3, c4, c5: floats
            Coefficients of the polynomial.

        Return
        ------
        Omega_m : float or None
            Return Omega_m value, or None.
        """
        import random

        maxiter = 100
        z = self.set_validate_range()

        coeff = (c2,c3,c4,c5)

        dL, d1L, d2L = pt.dL_poly(z, coeff, self.fit_distance,
                                         deriv=True)

        # Comoving distance
        d_co  = dL/(1.+z)
        d1_co = d1L/(1.+z) - np.power(1.+z,-2)*dL

        z0 = pt.sachs_z0(coeff, (z[0],z[-1]), self.fit_distance)

        if z0:
            Omega_m = pt.sachs_Omega_m(z0, coeff, self.fit_distance)
            h2_num = pt.sachs_h2_num(z, coeff, Omega_m,
                                            self.fit_distance)

            if np.any(h2_num < 0):
                Omega_m = None
        else:
            rand = np.random.rand(maxiter)
            for Omm in rand:
                h2_num = pt.sachs_h2_num(z, coeff, Omm,
                                                self.fit_distance)
                if np.all(h2_num > 0):
                    Omega_m = Omm
                    break
                else:
                    Omega_m = None

        return Omega_m
