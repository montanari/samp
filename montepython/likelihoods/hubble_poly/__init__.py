""".. module:: hubble_poly
    :synopsis: Polynomial likelihood for Hubble parameter data

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Fit Hubble parameter h(z)=H(z)/H0 data, assuming h(z) to be a
polynomial.

"""

import os
import numpy as np

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
import montepython.polytools as pt

class hubble_poly(Likelihood):
    """Base implementation of polynomial fit to H(z) measurements."""

    def __init__(self, path, data, command_line):
        Likelihood.__init__(self, path, data, command_line)

        # Read the content of the data file, containing z, Hz and error
        total = np.loadtxt(os.path.join(self.data_directory,
                                        self.data_file))

        # Store the columns separately
        self.z = total[:, 0]
        self.Hz_err = total[:, 2]
        if self.use_mock:
            self.Hz = self._get_mock_Hubble()
        else:
            self.Hz = total[:, 1]

    def loglkl(self, cosmo, data):
        """Main method."""
        coeff = self._get_coeff(data)
        H0 = self._get_H0(data)

        hz_poly = pt.hubble_poly(self.z,coeff)

        # Rescale to obtain adimensional Hubble parameter h(z)=H(z)/H0.
        self.hz_data = self.Hz / H0
        self.hz_err_data = self.Hz_err / H0

        chi2 = np.sum((self.hz_data - hz_poly)**2. / self.hz_err_data**2.)

        lkl = - 0.5 * chi2

        return lkl

    def _get_coeff(self, data):
        # Adapt here the number and name of coefficients
        p1 = (data.mcmc_parameters['p1']['current']
              *data.mcmc_parameters['p1']['scale'])
        p2 = (data.mcmc_parameters['p2']['current']
              *data.mcmc_parameters['p2']['scale'])
        # p3 = (data.mcmc_parameters['p3']['current']
        #       *data.mcmc_parameters['p3']['scale'])
        # coeff = (p1,p2,p3)
        coeff = (p1,p2)

        # How many parameters starting by 'p'?
        paramnames = data.get_mcmc_parameters(['nuisance'])
        np = 0
        for i, p in enumerate(paramnames):
            if p[0]=='p':
                np += 1
        if (np != len(coeff)):
            msg = ("If a polynomial of order different than %s is "
                   "implemented, this function must be changed "
                   "accordingly." % str(len(coeff)))
            raise RuntimeError(msg)

        return coeff

    def _get_H0(self, data):
        """Hubble constant."""
        H0 = (data.mcmc_parameters['H0']['current']
              * data.mcmc_parameters['H0']['scale'])
        return H0

    def _get_mock_Hubble(self):
        """Return LCDM Hubble parameter in km/s/Mpc."""

        Omega_m = 0.3
        Omega_Lambda = 0.7
        h = 0.7

        LambdaCDM = cc.LambdaCDM()
        cosmo = {'Omega_m': Omega_m,
                 'Omega_Lambda': Omega_Lambda,
                 'h': h}
        LambdaCDM.set_cosmo(cosmo)

        Hubble = LambdaCDM.H(self.z)

        return Hubble
