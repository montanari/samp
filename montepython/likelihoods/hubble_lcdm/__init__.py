import os
import numpy as np
from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
import montepython.polytools as pt

class hubble_lcdm(Likelihood):
    """Base implementation of flat LCDM fit to H(z) measurements."""

    def __init__(self, path, data, command_line):
        Likelihood.__init__(self, path, data, command_line)

        # Read the content of the data file, containing z, Hz and error
        total = np.loadtxt(os.path.join(self.data_directory,
                                        self.data_file))

        # Store the columns separately
        self.z = total[:, 0]
        self.Hz_err = total[:, 2]
        if self.use_mock:
            self.Hz = self._get_mock_Hubble()
        else:
            self.Hz = total[:, 1]

        self.LambdaCDM = cc.LambdaCDM()

    def loglkl(self, cosmo, data):
        """Main method."""
        # Retrieve current parameters
        Omega_m = (data.mcmc_parameters['Omega_m']['current']
                    * data.mcmc_parameters['Omega_m']['scale'])
        Omega_k = (data.mcmc_parameters['Omega_k']['current']
                    * data.mcmc_parameters['Omega_k']['scale'])
        H0 = (data.mcmc_parameters['H0']['current']
              * data.mcmc_parameters['H0']['scale'])

        # Update the cosmology
        lcdm = {'Omega_m': Omega_m,
                'Omega_k' : Omega_k,
                'h': H0/100.}
        self.LambdaCDM.set_cosmo(lcdm)

        # Compute the theoretical Hubble
        Hubble = self.LambdaCDM.H(self.z)
        hz_lcdm = Hubble / H0

        # Rescale data to obtain adimensional Hubble parameter
        # h(z)=H(z)/H0.
        self.hz_data = self.Hz / H0
        self.hz_err_data = self.Hz_err / H0

        # loglkl
        chi2 = np.sum((self.hz_data - hz_lcdm)**2. / self.hz_err_data**2.)
        loglkl = - 0.5 * chi2

        return loglkl

    def _get_mock_Hubble(self):
        """Return LCDM Hubble parameter in km/s/Mpc."""

        LambdaCDM = cc.LambdaCDM()
        mock = {'Omega_m': 0.3,
                'Omega_k' : 0.0,
                'h': 0.7}
        LambdaCDM.set_cosmo(mock)

        Hubble = LambdaCDM.H(self.z)

        return Hubble
