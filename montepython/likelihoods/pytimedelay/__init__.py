"""Compressed time-delay likelihood, eq. 26 of Suyu et al. (2013)
[arXiv:1208.6010]. This module requires Astropy.
"""

import os
from astropy.cosmology import FlatLambdaCDM
from astropy import units as u
import numpy as np
from montepython.likelihood_class import Likelihood


class pytimedelay(Likelihood):
    def __init__(self, path, data, command_line):
        # Always required by Monte Python.
        Likelihood.__init__(self, path, data, command_line)

        # Read data points.
        datafile = os.path.join(self.data_directory, self.fname)
        td_data = np.loadtxt(datafile).transpose()
        self.zd, self.zs, self.lambda_d, self.mu_d, self.sigma_d = td_data

    def loglkl(self, cosmo, data):
        # Current point in MCMC parameter space.
        H0 = (data.mcmc_parameters['H0']['current']
              * data.mcmc_parameters['H0']['scale'])
        Omega_m = (data.mcmc_parameters['Omega_m']['current']
                   * data.mcmc_parameters['Omega_m']['scale'])

        # Call Astropy. Distances are in Mpc.
        cosmo = FlatLambdaCDM(H0=H0, Om0=Omega_m, Tcmb0=2.725)
        Dd = cosmo.angular_diameter_distance(self.zd)
        Ds = cosmo.angular_diameter_distance(self.zs)
        Dds = (((1. + self.zs) * Ds - (1 + self.zd) * Dd)
               / ( 1. + self.zs))

        # Time delay distance dt = Dt / 1 Mpc.
        dt = (1 + self.zd) * Dd * Ds / Dds / u.Mpc

        # Compute likelihood.
        if np.all(np.greater(dt, self.lambda_d)):
            lkls = - ((np.log(dt - self.lambda_d) - self.mu_d) ** 2 /
                      2. / self.sigma_d ** 2)\
                  - (np.log(np.sqrt(2. * np.pi) * (dt - self.lambda_d)
                            * self.sigma_d))
            lkl = np.sum(lkls)
        else:
            lkl = data.boundary_loglike

        return lkl
