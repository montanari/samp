import operator
import os
import re

import numpy as np
from scipy import integrate
from scipy import interpolate

from montepython.cosmolib import cosmology as cc
from montepython.likelihood_class import Likelihood
import montepython.manipulate_strings as mstr
import montepython.splinetools as st


class hubble_spline(Likelihood):
    """Base implementation of polynomial fit to H(z) measurements."""

    def __init__(self, path, data, command_line):
        Likelihood.__init__(self, path, data, command_line)

        # Read the content of the data file, containing z, Hz and
        # error.
        total = np.loadtxt(os.path.join(self.data_directory,
                                        self.data_file))

        # Store the columns separately.
        self.z = total[:, 0]
        self.Hz_err = total[:, 2]
        if self.use_mock:
            self.Hz = self._get_mock_Hubble()
        else:
            self.Hz = total[:, 1]

        # Compute spline knots and save them on log.param.
        if self.dictionary['knots'] == 'auto':
            log_path = os.path.join(command_line.folder, 'log.param')
            self._set_knots(log_path)

    def loglkl(self, cosmo, data):
        """Main method."""
        spline_hubbles = self._get_spline_hubbles(data)
        H0 = self._get_H0(data)

        # Spline points to be fitted
        x = self.knots
        y = np.asarray(spline_hubbles)
        y_0 = 1.
        hz_spline, y2 = st.spline_p0(self.z, x, y, y_0)

        # Rescale to obtain adimensional Hubble parameter h(z)=H(z)/H0.
        self.hz_data = self.Hz / H0
        self.hz_err_data = self.Hz_err / H0

        chi2 = np.sum((self.hz_data - hz_spline)**2. / self.hz_err_data**2.)

        # Add roughness parameter. Remember that cubic splines have linear
        # second derivatives.
        if self.rough_lambda < 0.:
            raise RuntimeError('Roughness parameter must be non-negative')
        elif self.rough_lambda == 0.:
            pass
        else:
            self.h_prime_prime = interpolate.interp1d(x, y2, kind='slinear')
            result, __ = integrate.quad(self._rough_integrand, x[0], x[-1])
            rough = self.rough_lambda * result
            chi2 += rough

        lkl = - 0.5 * chi2

        return lkl

    def _set_knots(self,log_path):
        """Set the spline knots on a logarithmic scale.

        Parameters
        ----------
        log_path : string
                   path of the log.param file
        """
        # Sort z array in ascending order and read initial/final
        # redshifts.
        L = sorted(zip(self.z,self.Hz), key=operator.itemgetter(0))
        z_sorted, Hz_sorted = np.asarray(zip(*L))
        self.logz_start=np.log10(z_sorted[0])
        self.logz_stop=np.log10(z_sorted[-1])

        # Define increasing spline knots on log scale.
        self.knots = np.logspace(self.logz_start, self.logz_stop,
                                 num=len(self.spline_params), base=10.0)

        # Write the knots to the .data file to record correctly
        # log.param.
        karrstr = np.char.mod('%f', self.knots) # String array
        kstr = "["+", ".join(karrstr)+"]\n" # Combine to a string
        klabel = self.name+'.knots'
        mstr.replace(log_path, '(?m)^'+re.escape(klabel)+'.*\n?',
                     klabel+'='+kstr)

        # Update also the dictionary.
        self.dictionary['knots'] = self.knots

    def _get_spline_hubbles(self, data):
        """Retrieve the Hubble parameters at spline knots."""
        coeff = []
        for hname in self.spline_params:
            setname = ("{name} = (data.mcmc_parameters['{name}']['current']"
                       "* data.mcmc_parameters['{name}']['scale'])")
            exec(setname.format(name=hname))
            exec("coeff += [%s]" % hname)

        # Consistency check: how many parameters starting by 'h'?
        paramnames = data.get_mcmc_parameters(['nuisance'])
        np = 0
        for i, p in enumerate(paramnames):
            if p[0]=='h':
                np += 1
        if (np != len(coeff)):
            msg = ("If a spline knots number different than %s is "
                   "implemented, this function must be changed "
                   "accordingly." % str(len(coeff)))
            raise RuntimeError(msg)

        return coeff

    def _get_H0(self, data):
        """Hubble constant."""
        H0 = (data.mcmc_parameters['H0']['current']
              * data.mcmc_parameters['H0']['scale'])
        return H0

    def _get_mock_Hubble(self):
        """Return LCDM Hubble parameter in km/s/Mpc."""
        LambdaCDM = cc.LambdaCDM()
        cosmo = {'Omega_m': 0.3,
                 'Omega_Lambda': 0.7,
                 'h': 0.7}
        LambdaCDM.set_cosmo(cosmo)
        Hubble = LambdaCDM.H(self.z)
        return Hubble

    def _rough_integrand(self,z):
        """Integrand to compute the roughness factor."""
        # int (h''(ln z))**2 d ln z, where d ln z = dz/z
        return self.h_prime_prime(z)**2./z
