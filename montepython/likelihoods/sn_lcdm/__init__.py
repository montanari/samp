import os
import numpy as np
from montepython.likelihood_class import Likelihood
from scipy import integrate
import operator
import manipulate_strings as mstr
import re

class sn_lcdm(Likelihood):

    # initialization routine

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)

        # define array for values of z and data points
        self.z = np.array([], 'float64')
        self.moduli = np.array([], 'float64')

        # read redshifts and data points
        for line in open(os.path.join(
                self.data_directory, self.z_mu_dmu), 'r'):
            if (line.find('#') == -1):
                self.z = np.append(self.z, float(line.split()[1]))
                self.moduli = np.append(self.moduli, float(line.split()[2]))

        # number of data points
        self.num_points = np.shape(self.z)[0]

        # define correlation matrix
        covmat = np.zeros((self.num_points, self.num_points), 'float64')

        # file containing correlation matrix
        if self.has_syscovmat:
            covmat_filename = self.covmat_sys
        else:
            covmat_filename = self.covmat_nosys

        # read correlation matrix
        i = 0
        for line in open(os.path.join(
                self.data_directory, covmat_filename), 'r'):
            if (line.find('#') == -1):
                covmat[i] = line.split()
                i += 1

        # invert correlation matrix
        self.inv_covmat = np.linalg.inv(covmat)

        # find sum of all matrix elements (sounds odd that there is
        # not a trace here instead, but this is correct!)
        self.inv_covmat_sum = np.sum(self.inv_covmat)

        # end of initialization


    def comoving_distance_integrand_lcdm(self,z,Omega_m):
        return 1./np.sqrt(np.power(1.+z,3)*Omega_m + 1.-Omega_m)


    def comoving_distance_lcdm(self,z,Omega_m):
        return integrate.quad(self.comoving_distance_integrand_lcdm, 0, z, args=Omega_m)


    def loglkl(self, cosmo, data):
        """Compute the likelihood"""

        # Technically all parameters varying in the likelihood, but not in the Boltzmann code, are all nuisance parameters
        nuisance_parameter_names = data.get_mcmc_parameters(['nuisance'])

        # fill with nuisance parameters
        for nuisance in nuisance_parameter_names:
            if nuisance == 'Omega_m':
                nuisance_Omega_m = data.mcmc_parameters[nuisance]['current']*data.mcmc_parameters[nuisance]['scale']
                # index += 1

        # define array for difference between theory and observations
        difference = np.ndarray(self.num_points, 'float64')

        # # for each point, compute luminosity distance d_L=(1+z)**2d_A and infer
        # # theoretical prediction and difference with observation
        # for i in range(self.num_points):
            # d = cosmo.angular_distance(self.z[i])
            # difference[i] = 5 * np.log10((1 + self.z[i]) ** 2 * d) + 25 - self.moduli[i]

        for i in range(self.num_points):
            d, err = self.comoving_distance_lcdm(self.z[i], nuisance_Omega_m)
            difference[i] = 5 * np.log10((1 + self.z[i]) * d) \
                            + self.M - self.moduli[i]

        # chisquare before analytic marginalization
        AT = np.dot(difference, np.dot(self.inv_covmat, difference))

        # correct chi square with effect of analytic marginalization
        if self.has_marginalization:
            BT = np.sum(np.dot(self.inv_covmat, difference))
        else:
            BT = 0

        # final chi square
        chi_squared = AT - (BT ** 2) / self.inv_covmat_sum

        # return ln(L)
        lkl = - 0.5 * chi_squared

        return lkl
