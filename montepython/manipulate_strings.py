"""
.. module:: manipulate_strings
   :synopsis: Replace a pattern into a file.
.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Replace a pattern into a file.

"""

import re
import sys

def replace(file, pattern, subst):
    """Parse a file and replace a pattern.
    Useful regex expressions https://docs.python.org/2/library/re.html :
    '(?m)' --> enable multiline search
    '^'    --> line start
    '.*'   --> any sequence of character

    Parameters
    ----------
    file : string
    Files to be parsed
    patter : string
    Pattern to be replaced
    subst : string
    Replacement
    """
    # Read contents from file as a single string
    file_handle = open(file, 'r')
    file_string = file_handle.read()
    file_handle.close()

    # Use RE package to allow for replacement (also allowing for (multiline) REGEX)
    file_string = (re.sub(pattern, subst, file_string))

    # Write contents to file.
    # Using mode 'w' truncates the file.
    file_handle = open(file, 'w')
    file_handle.write(file_string)
    file_handle.close()


if __name__ == '__main__':
    path =  sys.argv[-3]
    pattern = sys.argv[-2]
    subst = sys.argv[-1]

    try:
        replace(path, pattern, subst)
    except ValueError:
        print("The arguments must be in the following form : file pattern "
              "replacement")
