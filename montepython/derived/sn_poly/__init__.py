"""
.. module:: sn_poly
    :synopsis: Add derived parameters to SN polynomial chains
.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>
.. versionadded:: 2.2.1its1

This module adds derived parameters depending on the polynomial
parameters defined in the sn_poly likelihood.

"""

from montepython.likelihoods.sn_poly import polytools as pt


class sn_poly:
    def __init__(self):
        pass


    def get_current_derived_parameters(self, data):
        """Give the value of all the derived parameters defined in the
        data class. This function should always be present in the
        module. Compute and define here all derived nuisance
        parameters.

        Parameters
        ----------
        data: class
            Contains a list of derived parameters
        """

        # Retrieve the current varying parameter, and compute the
        # derived parameters
        self._get_lkl_params(data)
        self._get_current_varying(data)
        self._set_params(self.coeff, self.limits, self.fitdist)

        # Add here all the derived nuisance parameters, once they have
        # been computed.
        for elem in data.get_mcmc_parameters(['derived_nuisance']):
            if elem == 'z0':
                data.mcmc_parameters[elem]['current'] = self.z0
            elif elem == 'Omega_m':
                data.mcmc_parameters[elem]['current'] = self.Omega_m
            else:
                raise RuntimeError("%s was not recognized as a derived\n"
                                   "nuisance parameter" % elem)
        return


    def _get_lkl_params(self, data):
        """Recover likelihood parameters set in the
        likelihoods/lkl/lkl.data file.
        """
        self.limits = data.lkl['sn_poly'].validation_range
        self.fitdist = data.lkl['sn_poly'].fit_distance


    def _get_current_varying(self, data):
        """Recover the current value of the varying parameters of the
        original chain.
        """
        varying_param_names = data.get_mcmc_parameters(['varying'])
        c4 = 0
        c5 = 0
        for varying in varying_param_names:
            if varying == 'c2':
                c2 = data.mcmc_parameters[varying]['current']\
                     *data.mcmc_parameters[varying]['scale']
            elif varying == 'c3':
                c3 = data.mcmc_parameters[varying]['current']\
                     *data.mcmc_parameters[varying]['scale']
            elif varying == 'c4':
                c4 = data.mcmc_parameters[varying]['current']\
                     *data.mcmc_parameters[varying]['scale']
            elif varying == 'c5':
                c5 = data.mcmc_parameters[varying]['current']\
                     *data.mcmc_parameters[varying]['scale']
        self.coeff = (c2, c3, c4, c5)


    def _set_params(self, coeff, limits, fitdist):
        """Compute the derived parameters."""
        self.z0 = pt.sachs_z0(coeff, limits, fitdist)
        self.Omega_m = pt.sachs_Omega_m(self.z0, coeff, fitdist)
