import unittest
import cosmology as cc
import numpy as np
from astropy.cosmology import LambdaCDM

class TestCosmologyDistance(unittest.TestCase):
    def setUp(self):
        self.redshifts = np.array([0.1,1.0,10.0])

        self.h = 0.70
        self.H0 = 100*self.h
        self.Omega_m=0.3

        # Neglect relativistic species
        self.Omega_r=0.
        self.Tcmb0=0.

        self.rtol = 1e-5 # Relative error tolerance


    def tearDown(self):
        pass


    def _get_astropy_dA(self,Omega_Lambda):
        """Get angular diameter distance from astropy"""
        cosmo = LambdaCDM(H0=self.H0,
                                  Om0=self.Omega_m,
                                  Ode0=Omega_Lambda,
                                  Tcmb0=self.Tcmb0)

        dAobj = cosmo.angular_diameter_distance(self.redshifts)
        dA = dAobj.value

        return dA


    def _get_dA_with_Lambda(self,Omega_Lambda):
        """Get angular diameter distance by passing Omega_Lambda
        (i.e., Omega_k set automatically).
        """
        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h':self.h,
                 'Omega_m':self.Omega_m,
                 'Omega_Lambda':Omega_Lambda}
        LambdaCDM.set_cosmo(cosmo)
        dA = LambdaCDM.angular_distance(self.redshifts)
        return dA


    def _get_dA_with_k(self,Omega_k):
        """Get angular diameter distance by passing Omega_k
        (i.e., Omega_Lambda set automatically).
        """
        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h':self.h,
                 'Omega_m':self.Omega_m,
                 'Omega_k':Omega_k}
        LambdaCDM.set_cosmo(cosmo)
        dA = LambdaCDM.angular_distance(self.redshifts)
        return dA


    def test_angular_distance(self):
        for Omega_Lambda in [0.5,0.7,0.9]:

            dA_astropy = self._get_astropy_dA(Omega_Lambda)

            dA_Lambda = self._get_dA_with_Lambda(Omega_Lambda)

            Omega_k = 1 - self.Omega_m - Omega_Lambda
            dA_k = self._get_dA_with_k(Omega_k)

            rerr_Lambda = abs(1.-dA_astropy/dA_Lambda)
            rerr_k = abs(1.-dA_astropy/dA_k)

            isgood_with_Lambda = np.any(rerr_Lambda < self.rtol)
            isgood_with_k = np.any(rerr_k < self.rtol)

            # Is distance recovered when setting Omega_Lambda?
            self.assertTrue(isgood_with_Lambda)

            # Is distance recovered when setting Omega_k?
            self.assertTrue(isgood_with_k)


class TestHubble(unittest.TestCase):
    def setUp(self):
        self.redshifts = np.array([0.1,1.0,10.0])

        self.h = 0.70
        self.H0 = 100*self.h
        self.Omega_m=0.3

        # Neglect relativistic species
        self.Omega_r=0.
        self.Tcmb0=0.

        self.rtol = 1e-5 # Relative error tolerance


    def tearDown(self):
        pass


    def _get_astropy_H(self,Omega_Lambda):
        """Get Hubble function from astropy"""
        cosmo = LambdaCDM(H0=self.H0,
                          Om0=self.Omega_m,
                          Ode0=Omega_Lambda,
                          Tcmb0=self.Tcmb0)

        Hobj = cosmo.H(self.redshifts)
        H = Hobj.value

        return H


    def _get_H_with_Lambda(self,Omega_Lambda):
        """Get Hubble function by passing Omega_Lambda (i.e., Omega_k
        set automatically).
        """
        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h':self.h,
                 'Omega_m':self.Omega_m,
                 'Omega_Lambda':Omega_Lambda}
        LambdaCDM.set_cosmo(cosmo)
        H = LambdaCDM.H(self.redshifts)
        return H


    def _get_H_with_k(self,Omega_k):
        """Get Hubble function by passing Omega_k (i.e., Omega_Lambda
        set automatically).
        """
        LambdaCDM = cc.LambdaCDM()
        cosmo = {'h':self.h,
                 'Omega_m':self.Omega_m,
                 'Omega_k':Omega_k}
        LambdaCDM.set_cosmo(cosmo)
        H = LambdaCDM.H(self.redshifts)
        return H


    def test_Hubble(self):
        for Omega_Lambda in [0.5,0.7,0.9]:

            H_astropy = self._get_astropy_H(Omega_Lambda)

            H_Lambda = self._get_H_with_Lambda(Omega_Lambda)

            Omega_k = 1 - self.Omega_m - Omega_Lambda
            H_k = self._get_H_with_k(Omega_k)

            rerr_Lambda = abs(1.-H_astropy/H_Lambda)
            rerr_k = abs(1.-H_astropy/H_k)

            isgood_with_Lambda = np.any(rerr_Lambda < self.rtol)
            isgood_with_k = np.any(rerr_k < self.rtol)

            # Is Hubble recovered when setting Omega_Lambda?
            self.assertTrue(isgood_with_Lambda)

            # Is Hubble recovered when setting Omega_k?
            self.assertTrue(isgood_with_k)


if __name__ == '__main__':
    unittest.main()
