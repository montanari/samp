"""
.. module:: cosmology
   :synopsis: Define functions of cosmology.

.. moduleauthor:: Francesco Montanari <francesco.montanari@helsinki.fi>

Collection of cosmological functions. Most of the functions here
defined can be called directly from the CLASS code. This module
provide an easier interface that allows easy identification of
functions definition.

"""

import numpy as np
import scipy.integrate as integrate

class LambdaCDM:
    """The notation follows Hoggs arXiv:9905116v4. Length units are Mpc.
    """
    def __init__(self):
        self.default_cosmo = {'Omega_m':0.3,
                              'Omega_Lambda':0.7,
                              'Omega_k':0.,
                              'Omega_r':0.,
                              'h':0.7}

        self.h_to_H0 = 1./2997.9 # Mpc

        for param in self.default_cosmo:
            if param == 'h':
                self.H0 = self.default_cosmo['h'] * self.h_to_H0
            else:
                exec("self.%s = self.default_cosmo['%s']"
                     %(param,param))



    def set_cosmo(self,cosmo):
        """Set the cosmological parameters.

        Parameters
        ----------
        cosmo : dict
            Dictionary containing the cosmological parameters.

        Example
        -------
        >>> import cosmolib.cosmology as cc
        >>> LambdaCDM = cc.LambdaCDM()
        >>> cosmo = {'Omega_m':0.3, 'Omega_Lambda':0.7}
        >>> LambdaCDM.set_cosmo(cosmo)
        """
        self.cosmo = cosmo

        for param in self.cosmo:
            if param not in self.default_cosmo:
                msg = ('Parameter %s not recognized.' % param)
                raise NotImplementedError(msg)

        if 'Omega_m' in self.cosmo:
            self.Omega_m = cosmo['Omega_m']

        if 'h' in self.cosmo:
            self.H0 = cosmo['h'] * self.h_to_H0 # Mpc

        case_k = ('Omega_k' in self.cosmo)
        case_Lambda = ('Omega_Lambda' in self.cosmo)

        if case_k and case_Lambda:
            msg = '''Please provide either Omega_k or Omega_Lambda,
            not both.'''
            raise RuntimeError(msg)
        elif case_k:
            self.Omega_k = cosmo['Omega_k']
            self.Omega_Lambda = 1 - self.Omega_k - self.Omega_m
        elif case_Lambda:
            self.Omega_Lambda = cosmo['Omega_Lambda']
            self.Omega_k = 1 - self.Omega_Lambda - self.Omega_m


    def get_cosmo_list(self):
        """Return an array of implemented cosmological parameters."""
        return self.default_cosmo.keys()


    def angular_distance(self, redshifts):
        """Angular diameter distance in Mpc. See Hoggs
        arXiv:astro-ph/9905116, eq. 18.

        Parameters
        ----------
        redshifts : array_like
            List of redshifts.

        Example
        -------
        >>> import cosmolib.cosmology as cc
        >>> LambdaCDM = cc.LambdaCDM()
        >>> cosmo = {'Omega_m':0.3, 'Omega_Lambda':0.7}
        >>> LambdaCDM.set_cosmo(cosmo)
        >>> print LambdaCDM.angular_distance([0.5,1.0,2.0])
        """
        redshifts = np.asarray(redshifts)

        dA = np.zeros(len(redshifts))
        for i,z in enumerate(redshifts):
            dA[i] = integrate.quad(self._invefunc, 0., z)[0]

        if self.Omega_k > 0:
            sqrtOmk = np.sqrt(self.Omega_k)
            dA = np.sinh(sqrtOmk*dA) / sqrtOmk
        elif self.Omega_k < 0:
            sqrtOmk = np.sqrt(-self.Omega_k)
            dA = np.sin(sqrtOmk*dA) / sqrtOmk

        dA = dA / (1.+redshifts) / self.H0

        return dA


    def efunc(self, z):
        """E(z) function Hoggs arXiv:astro-ph/9905116, eq. 14.

        Parameters
        ----------
        z : float or array_like
            Redshift.

        Returns
        -------
        E : float or array_like
            E(z) function.
        """
        zp1 = z+1.

        Ez = np.sqrt(self.Omega_r * zp1**4.
                         + self.Omega_m * zp1**3.
                         + self.Omega_k * zp1**2.
                         + self.Omega_Lambda)
        return Ez


    def _invefunc(self, z):
        """Inverse of Hoggs arXiv:astro-ph/9905116, eq. 14.

        Parameters
        ----------
        z : float or array_like
            Redshift.

        Returns
        -------
        inv_Ez : float or array_like
            Inverse E(z) function.
        """
        inv_Ez = 1./self.efunc(z)

        return inv_Ez


    def H(self, redshifts):
        """Hubble parameter in km/s/Mpc.

        Parameters
        ----------
        redshifts : array_like
            List of redshifts.

        Returns
        -------
        Hz : array_like
            H(z) function at the required redshifts.
        """
        redshifts = np.asarray(redshifts)

        H0_kmsMpc = (100*self.H0/self.h_to_H0)

        Hz = self.efunc(redshifts)*H0_kmsMpc

        return Hz
